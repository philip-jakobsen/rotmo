#!/bin/bash
#set -x
################################################################################
#                                                                              #
# Shell-script for running own program (gamess_basopt) with the SLURM queing   #
# system                                                                       #
# Using openMP                                                                 #
#  2/2022 PHJ                                                                  #
################################################################################
#
paramlist="$*"
#
#default values for options
BIN=/home/philip/speciale/gamess_basopt
export BIN
NCORES=0
NODES=1
MEM=0
TIME=1:00:00
QUEUE=q16
RESTARTSWEEP=0
REFDENS=0
FRJ_OPT=0
optfat=0
# 
# 
usage (){
          echo
          echo "  Usage:  phjprog [-np nodes] [-nc cores] [-t time] [-q queue] xxx"
          echo
          echo "  The input  files are assumed to have the name xxx.mol and xxx.dal"
          echo "  The output file is returned as xxx.out in the current directory"
          echo "  Modified for SLURM 10/2018"
          echo '  Options:'
          echo '  -q queue   (q12,q12fat,q16,q16l,q20,q20,q28,q36,qexp)        [default = q16]'
          echo '  -np nodes  number of nodes                  [default = 1]'
	  echo '  -t time    timelimit in cpu hours           [default = 1 hour]'
          echo '  -m         memory in gb                     [default = 48, 60, 60, 128, 256, 384 gb for the above queues]'
          echo '  -fat       submit to the 'fat' q12 and q20 nodes [default = no, if yes, then mem = 96 and 256]'
          echo '  -nc        number of cores on each node     [default = all cores]'
          echo '  -f         copy xxx.tar.gz file for restart or moread     '
          echo
         }

while [ -n "`echo "$1" | grep '-'`" ]; do
    case $1 in
      -fs ) RESTARTSWEEP=1;;
      -fd ) REFDENS=1;;
      -fo ) FRJ_OPT=1;;
      -fat ) optfat=1;;
      -q ) QUEUE=$2
           shift;;
      -t ) TIME="$2":00:00
           shift;;
      -m ) MEM=$2
           shift;;
      * ) usage; exit 1;;
   esac
   shift
done


#done
# 
# check for correct input
#if [ -z "$1" ]; then
##  echo 'Input file not specified'
   #usage
   #exit 1
#fi
#if [ ! -e $1.mol ] ; then
  #echo "MOL file not found -- aborting" 
  #exit 1
#else
  #MOL=$1
#fi
#if [ ! -e $1.dal ] ; then
  #echo "DAL file not found -- aborting" 
  #exit 1
#else
  #DAL=$1
#fi
##
#if [ $RESTART = 1 ] ; then
  #if [ ! -e $1.tar.gz ] ; then
    #echo "RESTART requested, but no $1.tar.gz file found -- aborting" 
    #exit 1
  #fi
#fi
#


#
#  check for OUT file
if [ -e $1.out ] ; then
  echo "Moving OUT file to backup"
  mv $1.out $1.out.back
fi

#  check for REFDENS file
if [[ -e $1.REFDENS && $REFDENS == 0 ]] ; then
  echo "Moving REFDENS file to backup"
  mv $1.REFDENS $1.REFDENS.back
fi
#  check for converged alphas fort.21 file
if [[ -e $1.fort.21 && $RESTARTSWEEP == 0 ]] ; then
  echo "Moving converged alphas fort.21 file to backup"
  mv $1.fort.21 $1.fort.21.back
fi

#

if [ $QUEUE = q8 ] ; then
     NCORES=8
fi
if [ $QUEUE = q12 ] ; then
     NCORES=12
fi
if [ $QUEUE = q12fat ] ; then
     NCORES=12
fi
if [ $QUEUE = q16 ] ; then
     NCORES=16
fi
if [ $QUEUE = q16l ] ; then
     NCORES=16
fi
if [ $QUEUE = q20 ] ; then
     NCORES=20
fi
if [ $QUEUE = q24 ] ; then
     NCORES=24
fi
if [ $QUEUE = q28 ] ; then
     NCORES=28
fi
if [ $QUEUE = qtest ] ; then
     NCORES=24
fi
if [ $QUEUE = q36 ] ; then
     NCORES=36
fi
if [ $QUEUE = qfat ] ; then
     NCORES=40
fi
if [ $QUEUE = q40 ] ; then
     NCORES=40
fi


# total number of cpus

NCPUS=$(($NCORES * $NODES))

# create submit script
#
echo "#!/bin/bash"                 > runphjprog.$1
echo "#SBATCH --job-name="$1"  "   >> runphjprog.$1 
echo "#SBATCH --nodes="$NODES" "   >> runphjprog.$1 
echo "#SBATCH --time="$TIME"     " >> runphjprog.$1 
echo "#SBATCH --exclusive"         >> runphjprog.$1 
echo "#SBATCH --partition=$QUEUE"  >> runphjprog.$1 
echo "#SBATCH --mem=0"             >> runphjprog.$1

# load stuff

echo "module load gcc"           >> runphjprog.$1
#echo "module load intel"           >> runphjprog.$1
#echo "module load openmpi"         >> runphjprog.$1

#echo "export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK=-1}" >> runphjprog.$1

echo "export OMP_NUM_THREADS=$NCORES" >> runphjprog.$1

# cd scratch
echo "pwd" >> runphjprog.$1
echo "echo "rotmo.x compiled at:"" >> runphjprog.$1
echo "ls -lth /home/philip/speciale/program/rotmo.x" >> runphjprog.$1


# cp rotmo executable
echo "cp /home/philip/speciale/program/rotmo.x /scratch/"'$SLURM_JOB_ID'"/rotmo.x " >> runphjprog.$1

# cp input file
echo "cp  "$1".inp /scratch/"'$SLURM_JOB_ID'"/"$1".inp" >> runphjprog.$1

# cp data files, found with grep
#echo "/bin/grep 'gamess/' "$1".inp > lst " >> runphjprog.$1 

#echo "cat lst" >> runphjprog.$1

# if load REFDENS, cp $1.REFDENS
if [ $REFDENS = 1 ]; then
    echo "Restart of REFDENS requested, copying $1.REFDENS to REFDENS"
    echo "cp "$1".REFDENS /scratch/"'$SLURM_JOB_ID'"/REFDENS" >> runphjprog.$1
fi

# if restart sweep, cp $1.fort.21
if [ $RESTARTSWEEP = 1 ]; then
    echo "Restart of alphas requested, copying $1.fort.21 to fort.90"
    echo "cp "$1".fort.21 /scratch/"'$SLURM_JOB_ID'"/fort.90" >> runphjprog.$1
fi

# if frj_opt, cp alphas (fort.30) as input
if [ $FRJ_OPT = 1 ]; then
    echo "FRJ_OPT = TRUE, copying fort.10 to fort.10"
    echo "cp fort.10 /scratch/"'$SLURM_JOB_ID'"/fort.10" >> runphjprog.$1
    # also popt.lst
    #echo "cp popt.lst.rotmo /scratch/"'$SLURM_JOB_ID'"/popt.lst.rotmo" >> runphjprog.$1
fi

#echo "while read l; do " >> runphjprog.$1
##echo "rsync -rv "'$SLURM_SUBMIT_DIR'"/./"'$l'" ." >> runphjprog.$1
#echo "cp --parents "'$l'" /scratch/"'$SLURM_JOB_ID'" " >> runphjprog.$1
#echo "done < lst " >> runphjprog.$1

echo "rm lst"

echo "cd /scratch/"'$SLURM_JOB_ID'" " >> runphjprog.$1

# watch for time limit
#echo "strigger --set --jobid="'$SLURM_JOB_ID'" --time=-120 --program /home/philip/"

echo "./rotmo.x < "$1".inp > "'$SLURM_SUBMIT_DIR'"/$1.out " >> runphjprog.$1

# cp back converged alphas
echo "cp fort.21 "'$SLURM_SUBMIT_DIR'"/"$1".fort.21 " >> runphjprog.$1

# cp back history of alphas
echo "cp fort.22 "'$SLURM_SUBMIT_DIR'"/"$1".fort.22 " >> runphjprog.$1

echo "cp fort.7 "'$SLURM_SUBMIT_DIR'"/"$1".fort.7 " >> runphjprog.$1

# converged UMO
echo "cp fort.99 "'$SLURM_SUBMIT_DIR'"/"$1".fort.99 " >> runphjprog.$1

# conv plot files
echo "cp fort.40 "'$SLURM_SUBMIT_DIR'"/"$1".fort.40 " >> runphjprog.$1

# if frj_opt, cp back gradient and error as output (fort.31 and fort.32)
if [ $FRJ_OPT = 1 ]; then
    echo "FRJ_OPT = TRUE, copying back fort.12 and fort.9"
    echo "cp fort.12 "'$SLURM_SUBMIT_DIR'"/fort.12" >> runphjprog.$1
    echo "cp fort.9 "'$SLURM_SUBMIT_DIR'"/fort.9" >> runphjprog.$1
fi

# cp back refdens only if not loaded from external file
if [ $REFDENS = 0 ]; then
    echo "cp fort.19 "'$SLURM_SUBMIT_DIR'"/"$1".REFDENS " >> runphjprog.$1
fi 

echo "ls -lthrs " >> runphjprog.$1
echo "tree" >> runphjprog.$1

# clean up
echo "rm -r gamess" >> runphjprog.$1




# 
chmod +x runphjprog.$1


sbatch runphjprog.$1
