! PHJ
!
! found at https://sites.google.com/site/kennyandersonfortranprograms/fit
!
!=====
!
!    This file is part of Fit.
!
!    Fit  is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    Fit is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with Fit.  If not, see <http://www.gnu.org/licenses/>.
!
!
!    This LSQ module is made from code was written by Allen Miller.
!    Code obtained from: http://jblevins.org/mirror/amiller/
!
!    Code was added at the bottom by Kenny Anderson in August, 2010 based
!    on a polynomial routine originally written by Allen Miller.
!
!    Allen Miller did not specify licensing.  Therefore, the GNU LGPL is 
!    applied.
!
module LSQ
!
!  Author: Allen Miller
!
implicit none
public       :: STARTUP, INCLUD, REGCF, TOLSET, SING, SS, COV, PARTIAL_CORR, &
VMOVE, REORDR, HDIAG, VARPRD, BKSUB2
private      :: INV
integer, save, public     :: NOBS, NCOL, R_DIM
integer, allocatable, save, dimension(:), public  :: VORDER, ROW_PTR
logical, save, public     :: INITIALIZED = .false.,    &
     TOL_SET = .false.,  &
     RSS_SET = .false.
integer, parameter, public      :: DP = selected_real_kind(10,70)
real (kind=DP), allocatable, save, dimension(:), public :: D, RHS, R, TOL, RSS
real (kind=DP), save, private   :: ZERO = 0.0_DP, ONE = 1.0_DP, VSMALL
real (kind=DP), save, public    :: SSERR, TOLY
 
contains
 
subroutine STARTUP(NVAR, FIT_CONST)
!
!  Author: Allen Miller
!
integer, intent(in)  :: NVAR
logical, intent(in)  :: FIT_CONST
integer  :: I
VSMALL = 10.0_DP * tiny(ZERO)
NOBS = 0
if (FIT_CONST) then
  NCOL = NVAR + 1
else
  NCOL = NVAR
end if
 
if (INITIALIZED) then
  deallocate(D, RHS, R, TOL, RSS, VORDER, ROW_PTR)
end if
R_DIM = NCOL * (NCOL - 1)/2
allocate( D(NCOL), RHS(NCOL), R(R_DIM), TOL(NCOL), RSS(NCOL), VORDER(NCOL),  &
ROW_PTR(NCOL) )
D = ZERO 
RHS = ZERO 
R = ZERO 
SSERR = ZERO  
 
if (FIT_CONST) then
  do I = 1, NCOL 
    VORDER(I) = I-1
  end do 
else 
  do I = 1, NCOL 
    VORDER(I) = I 
  end do
end if ! (fit_const) 
 
! row_ptr(i) is the position of element R(i,i+1) in array r(). 
 
ROW_PTR(1) = 1 
do I = 2, NCOL-1 
  ROW_PTR(I) = ROW_PTR(I-1) + NCOL - I + 1
end do
ROW_PTR(NCOL) = 0
 
INITIALIZED = .true.
TOL_SET = .false.
RSS_SET = .false.
 
return 
end subroutine STARTUP
 
subroutine INCLUD(WEIGHT, XROW, YELEM) 
!
!  Author: Allen Miller
!
real (kind=DP), intent(in)   :: WEIGHT, YELEM
real (kind=DP), dimension(:), intent(in out) :: XROW
 
!     Local variables 
 
integer         :: I, K, NEXTR
real (kind=DP)  :: W, Y, XI, DI, WXI, DPI, CBAR, SBAR, XK 
 
NOBS = NOBS + 1 
W = WEIGHT 
Y = YELEM
RSS_SET = .false.
NEXTR = 1 
do I = 1, NCOL
 
!     Skip unnecessary transformations.   Test on exact zeroes must be
!     used or stability can be destroyed. 
 
  if (abs(W) < VSMALL) then
    return 
  end if
  XI = XROW(I) 
  if (abs(XI) < VSMALL) then
    NEXTR = NEXTR + NCOL - I
  else
    DI = D(I) 
    WXI = W * XI
    DPI = DI + WXI*XI
    CBAR = DI / DPI 
    SBAR = WXI / DPI
    W = CBAR * W 
    D(I) = DPI
    do K = I+1, NCOL
      XK = XROW(K) 
      XROW(K) = XK - XI * R(NEXTR)
      R(NEXTR) = CBAR * R(NEXTR) + SBAR * XK
      NEXTR = NEXTR + 1 
    end do
    XK = Y 
    Y = XK - XI * RHS(I)
    RHS(I) = CBAR * RHS(I) + SBAR * XK
  end if
end do ! i = 1, ncol
 
SSERR = SSERR + W * Y * Y
 
return 
end subroutine INCLUD 
 
subroutine REGCF(BETA, NREQ, IFAULT) 
!
!  Author: Allen Miller
!
integer, intent(in):: NREQ  
integer, intent(out)       :: IFAULT 
real (kind=DP), dimension(:), intent(out)  :: BETA
 
!     Local variables
 
integer     :: I, J, NEXTR
 
!     Some checks.
 
IFAULT = 0 
if (NREQ < 1 .or. NREQ > NCOL) then
  IFAULT = IFAULT + 4 
end if 
if (IFAULT /= 0) then
  return
end if
 
if (.not. TOL_SET) then
  call TOLSET() 
end if
 
do I = NREQ, 1, -1
  if (sqrt(D(I)) < TOL(I)) then
    BETA(I) = ZERO
    D(I) = ZERO  
    IFAULT = -I
  else
    BETA(I) = RHS(I)
    NEXTR = ROW_PTR(I)
    do J = I+1, NREQ
      BETA(I) = BETA(I) - R(NEXTR) * BETA(J) 
      NEXTR = NEXTR + 1
    end do ! j = i+1, nreq  
  end if
end do ! i = nreq, 1, -1  
 
return 
end subroutine REGCF
 
subroutine TOLSET(EPS) 
!
!  Author: Allen Miller
!
real (kind=DP), intent(in), optional :: EPS 
 
 
integer:: COL, ROW, POS
real (kind=DP)   :: EPS1, TOTAL  
real (kind=DP), dimension(NCOL)  :: WORK
real (kind=DP), parameter        :: TEN = 10.0_DP 
 
!     EPS is a machine-dependent constant. 
 
if (present(EPS)) then 
  EPS1 = max(abs(EPS), TEN * epsilon(TEN)) 
else  
  EPS1 = TEN * epsilon(TEN) 
end if
 
!     Set tol(i) = sum of absolute values in column I of R after scaling each
!     element by the square root of its row multiplier, multiplied by EPS1. 
 
WORK = sqrt(D)  
do COL = 1, NCOL
  POS = COL - 1 
  TOTAL = WORK(COL) 
  do ROW = 1, COL-1 
    TOTAL = TOTAL + abs(R(POS)) * WORK(ROW) 
    POS = POS + NCOL - ROW - 1
  end do
  TOL(COL) = EPS1 * TOTAL
end do  
 
TOL_SET = .true. 
return
end subroutine TOLSET 
 
subroutine SING(LINDEP, IFAULT) 
!
!  Author: Allen Miller
!
integer, intent(out):: IFAULT 
logical, dimension(:), intent(out)  :: LINDEP 
 
!     Local variables
 
real (kind=DP)   :: TEMP, Y, WEIGHT  
integer:: COL, POS, ROW, POS2
real (kind=DP), dimension(NCOL)  :: X, WORK
 
IFAULT = 0 
 
WORK = sqrt(D)  
if (.not. TOL_SET) then
  call TOLSET()
end if 
 
do COL = 1, NCOL
  TEMP = TOL(COL)
  POS = COL - 1 
  do ROW = 1, COL-1  
    POS = POS + NCOL - ROW - 1 
  end do
 
!     If diagonal element is near zero, set it to zero, set appropriate
!     element of LINDEP, and use INCLUD to augment the projections in
!     the lower rows of the orthogonalization. 
 
  LINDEP(COL) = .false.  
  if (WORK(COL) <= TEMP) then
    LINDEP(COL) = .true. 
    IFAULT = IFAULT - 1
    if (COL < NCOL) then
      POS2 = POS + NCOL - COL + 1
      X = ZERO 
      X(COL+1:NCOL) = R(POS+1:POS2-1)
      Y = RHS(COL) 
      WEIGHT = D(COL)
      R(POS+1:POS2-1) = ZERO 
      D(COL) = ZERO 
      RHS(COL) = ZERO
      call INCLUD(WEIGHT, X, Y) 
     ! INCLUD automatically increases the number 
     ! of cases each time it is called. 
      NOBS = NOBS - 1 
    else 
      SSERR = SSERR + D(COL) * RHS(COL)**2
    end if ! (col < ncol)
  end if ! (work(col) <= temp) 
end do ! col = 1, ncol
return 
end subroutine SING
 
subroutine SS()
!
!  Author: Allen Miller
!
integer:: I
real (kind=DP)   :: TOTAL 
 
TOTAL = SSERR
RSS(NCOL) = SSERR 
do I = NCOL, 2, -1
  TOTAL = TOTAL + D(I) * RHS(I)**2
  RSS(I-1) = TOTAL
end do 
 
RSS_SET = .true. 
return 
end subroutine SS
 
subroutine COV(NREQ, VAR, COVMAT, DIMCOV, STERR, IFAULT)
!
!  Author: Allen Miller
!
integer, intent(in):: NREQ, DIMCOV 
integer, intent(out)       :: IFAULT 
real (kind=DP), intent(out):: VAR 
real (kind=DP), dimension(:), intent(out)  :: COVMAT, STERR
!     Local variables. 
integer :: DIM_RINV, POS, ROW, START, POS2, COL, POS1, K 
real (kind=DP)    :: TOTAL  
real (kind=DP), allocatable, dimension(:)  :: RINV 
!     Check that dimension of array covmat is adequate.  
if (DIMCOV < NREQ*(NREQ+1)/2) then 
  IFAULT = 1
  return 
end if
!     Check for small or zero multipliers on the diagonal. 
IFAULT = 0
do ROW = 1, NREQ  
  if (abs(D(ROW)) < VSMALL) then
    IFAULT = -ROW 
  end if
end do 
if (IFAULT /= 0) then
  return
end if
!     Calculate estimate of the residual variance.
if (NOBS > NREQ) then
  if (.not. RSS_SET) then
    call SS() 
  end if
  VAR = RSS(NREQ) / (NOBS - NREQ) 
else
  IFAULT = 2
  return 
end if 
 
DIM_RINV = NREQ*(NREQ-1)/2
allocate ( RINV(DIM_RINV) )
 
call INV(NREQ, RINV)
POS = 1 
START = 1
do ROW = 1, NREQ
  POS2 = START
  do COL = ROW, NREQ
    POS1 = START + COL - ROW 
    if (ROW == COL) then 
      TOTAL = ONE / D(COL) 
    else 
      TOTAL = RINV(POS1-1) / D(COL) 
    end if
    do K = COL+1, NREQ
      TOTAL = TOTAL + RINV(POS1) * RINV(POS2) / D(K) 
      POS1 = POS1 + 1
      POS2 = POS2 + 1
    end do
    COVMAT(POS) = TOTAL * VAR
    if (ROW == COL) then 
      STERR(ROW) = sqrt(COVMAT(POS)) 
    end if
    POS = POS + 1
  end do
  START = START + NREQ - ROW 
end do
 
deallocate(RINV)
return 
end subroutine COV
 
subroutine INV(NREQ, RINV) 
!
!  Author: Allen Miller
!
integer, intent(in):: NREQ 
real (kind=DP), dimension(:), intent(out)  :: RINV
 
!     Local variables. 
 
integer:: POS, ROW, COL, START, K, POS1, POS2 
real (kind=DP)   :: TOTAL
!     Invert R ignoring row multipliers, from the bottom up. 
 
POS = NREQ * (NREQ-1)/2 
do ROW = NREQ-1, 1, -1 
  START = ROW_PTR(ROW) 
  do COL = NREQ, ROW+1, -1
    POS1 = START
    POS2 = POS 
    TOTAL = ZERO
    do K = ROW+1, COL-1 
      POS2 = POS2 + NREQ - K 
      TOTAL = TOTAL - R(POS1) * RINV(POS2)  
      POS1 = POS1 + 1 
    end do ! k = row+1, col-1 
    RINV(POS) = TOTAL - R(POS1) 
    POS = POS - 1  
  end do ! col = nreq, row+1, -1 
end do ! row = nreq-1, 1, -1 
 
return
end subroutine INV
 
subroutine PARTIAL_CORR(INVAR, CORMAT, DIMC, YCORR, IFAULT)
!
!  Author: Allen Miller
!
integer, intent(in):: INVAR, DIMC 
integer, intent(out)       :: IFAULT 
real (kind=DP), dimension(:), intent(out)  :: CORMAT, YCORR
!     Local variables. 
integer:: BASE_POS, POS, ROW, COL, COL1, COL2, POS1, POS2 
real (kind=DP)   :: SUMXX, SUMXY, SUMYY
real (kind=DP), dimension(INVAR+1:NCOL)  :: RMS, WORK
 
!     Some checks. 
 
IFAULT = 0
if (INVAR < 0 .or. INVAR > NCOL-1) then 
  IFAULT = IFAULT + 4
end if
if (DIMC < (NCOL-INVAR)*(NCOL-INVAR-1)/2) then  
  IFAULT = IFAULT + 8
end if
if (IFAULT /= 0) then
  return
end if
 
BASE_POS = INVAR*NCOL - (INVAR+1)*(INVAR+2)/2  
 
if (D(INVAR+1) > ZERO) then
  RMS(INVAR+1) = ONE / sqrt(D(INVAR+1))
end if 
do COL = INVAR+2, NCOL
  POS = BASE_POS + COL
  SUMXX = D(COL) 
  do ROW = INVAR+1, COL-1
    SUMXX = SUMXX + D(ROW) * R(POS)**2 
    POS = POS + NCOL - ROW - 1 
  end do ! row = INVAR+1, col-1
  if (SUMXX > ZERO) then
    RMS(COL) = ONE / sqrt(SUMXX)
  else 
    RMS(COL) = ZERO 
    IFAULT = -COL
  end if
end do  
 
SUMYY = SSERR 
do ROW = INVAR+1, NCOL 
  SUMYY = SUMYY + D(ROW) * RHS(ROW)**2 
end do ! row = INVAR+1, ncol
if (SUMYY > ZERO) then
  SUMYY = ONE / sqrt(SUMYY) 
end if
 
POS = 1
do COL1 = INVAR+1, NCOL
  SUMXY = ZERO
  WORK(COL1+1:NCOL) = ZERO
  POS1 = BASE_POS + COL1
  do ROW = INVAR+1, COL1-1 
    POS2 = POS1 + 1
    do COL2 = COL1+1, NCOL
      WORK(COL2) = WORK(COL2) + D(ROW) * R(POS1) * R(POS2)  
      POS2 = POS2 + 1
    end do ! col2 = col1+1, ncol 
    SUMXY = SUMXY + D(ROW) * R(POS1) * RHS(ROW) 
    POS1 = POS1 + NCOL - ROW - 1 
  end do ! row = INVAR+1, col1-1
!     Row COL1 has an implicit 1 as its first element (in column COL1) 
 
  POS2 = POS1 + 1 
  do COL2 = COL1+1, NCOL 
    WORK(COL2) = WORK(COL2) + D(COL1) * R(POS2)
    POS2 = POS2 + 1 
    CORMAT(POS) = WORK(COL2) * RMS(COL1) * RMS(COL2)  
    POS = POS + 1
  end do ! col2 = col1+1, ncol
  SUMXY = SUMXY + D(COL1) * RHS(COL1) 
  YCORR(COL1) = SUMXY * RMS(COL1) * SUMYY 
end do ! col1 = INVAR+1, ncol-1
 
YCORR(1:INVAR) = ZERO
 
return  
end subroutine PARTIAL_CORR
 
subroutine VMOVE(FROM, DEST, IFAULT)
!
!  Author: Allen Miller
!
integer, intent(in)    :: FROM, DEST  
integer, intent(out)   :: IFAULT
 
!     Local variables
 
real (kind=DP)   :: D1, D2, X, D1NEW, D2NEW, CBAR, SBAR, Y  
integer:: M, FIRST, LAST, INC, M1, M2, MP1, COL, POS, ROW 
 
!     Check input parameters 
 
IFAULT = 0 
if (FROM < 1 .or. FROM > NCOL) then 
  IFAULT = IFAULT + 4  
end if
if (DEST < 1 .or. DEST > NCOL) then
  IFAULT = IFAULT + 8 
end if 
if (IFAULT /= 0) then 
  return
end if
 
if (FROM == DEST) then
  return
end if
 
if (.not. RSS_SET) then 
  call SS() 
end if
 
if (FROM < DEST) then  
  FIRST = FROM 
  LAST = DEST - 1
  INC = 1 
else
  FIRST = FROM - 1 
  LAST = DEST
  INC = -1 
end if 
 
do M = FIRST, LAST, INC  
 
!     Find addresses of first elements of R in rows M and (M+1).
 
  M1 = ROW_PTR(M)
  M2 = ROW_PTR(M+1) 
  MP1 = M + 1 
  D1 = D(M)
  D2 = D(MP1) 
 
!     Special cases.  
 
  if (D1 >= VSMALL .or. D2 >= VSMALL) then
    X = R(M1) 
    if (abs(X) * sqrt(D1) < TOL(MP1)) then
      X = ZERO
    end if 
    if (D1 < VSMALL .or. abs(X) < VSMALL) then
      D(M) = D2
      D(MP1) = D1
      R(M1) = ZERO 
      do COL = M+2, NCOL  
        M1 = M1 + 1
        X = R(M1) 
        R(M1) = R(M2)
        R(M2) = X 
        M2 = M2 + 1
      end do 
      X = RHS(M)
      RHS(M) = RHS(MP1)
      RHS(MP1) = X
    else if (D2 < VSMALL) then
      D(M) = D1 * X**2
      R(M1) = ONE / X  
      R(M1+1:M1+NCOL-M-1) = R(M1+1:M1+NCOL-M-1) / X 
      RHS(M) = RHS(M) / X
    else
 
!     Planar rotation in regular case. 
 
      D1NEW = D2 + D1*X**2 
      CBAR = D2 / D1NEW 
      SBAR = X * D1 / D1NEW
      D2NEW = D1 * CBAR 
      D(M) = D1NEW
      D(MP1) = D2NEW
      R(M1) = SBAR
      do COL = M+2, NCOL
        M1 = M1 + 1
        Y = R(M1) 
        R(M1) = CBAR*R(M2) + SBAR*Y
        R(M2) = Y - X*R(M2)
        M2 = M2 + 1
      end do 
      Y = RHS(M) 
      RHS(M) = CBAR*RHS(MP1) + SBAR*Y
      RHS(MP1) = Y - X*RHS(MP1)
    end if
  end if
 
!     Swap columns M and (M+1) down to row (M-1)
 
  POS = M 
  do ROW = 1, M-1
    X = R(POS) 
    R(POS) = R(POS-1) 
    R(POS-1) = X
    POS = POS + NCOL - ROW - 1
  end do ! row = 1, m-1
 
!     Adjust variable order (VORDER), the tolerances (TOL) and
!     the vector of residual sums of squares (RSS).  
 
  M1 = VORDER(M) 
  VORDER(M) = VORDER(MP1) 
  VORDER(MP1) = M1 
  X = TOL(M) 
  TOL(M) = TOL(MP1) 
  TOL(MP1) = X 
  RSS(M) = RSS(MP1) + D(MP1) * RHS(MP1)**2
end do
 
return
end subroutine VMOVE 
 
 
subroutine REORDR(LIST, N, POS1, IFAULT) 
!
!  Author: Allen Miller
!
integer, intent(in):: N, POS1 
integer, dimension(:), intent(in)  :: LIST 
integer, intent(out)   :: IFAULT
 
!     Local variables.
 
integer     :: NEXT, I, L, J
logical     :: FOUND
 
!     Check N. 
 
IFAULT = 0
if (N < 1 .or. N > NCOL+1-POS1) then
  IFAULT = IFAULT + 4 
end if
if (IFAULT /= 0) then
  return
end if
 
!     Work through VORDER finding variables which are in LIST.  
 
NEXT = POS1  
do I = POS1, NCOL 
  L = VORDER(I)
  FOUND = .false.
  do J = 1, N
    if (L == LIST(J)) then 
      FOUND = .true.
      exit 
    end if 
  end do
 
!     Variable L is in LIST; move it up to position NEXT if it is not 
!     already there. 
 
  if (FOUND) then
    if (I > NEXT) then 
      call VMOVE(I, NEXT, IFAULT)  
    end if 
    NEXT = NEXT + 1
  end if
end do 
 
if (NEXT >= N+POS1) then  
  return
end if
 
!     If this point is reached, one or more variables in LIST has not
!     been found. 
 
IFAULT = 8 
 
return 
end subroutine REORDR 
 
subroutine HDIAG(XROW, NREQ, HII, IFAULT) 
!
!  Author: Allen Miller
!
integer, intent(in)       :: NREQ 
integer, intent(out)      :: IFAULT
real (kind=DP), dimension(:), intent(in)  :: XROW  
real (kind=DP), intent(out)   :: HII 
 
!     Local variables 
 
integer         :: COL, ROW, POS  
real (kind=DP)  :: TOTAL
real (kind=DP), dimension(NCOL)  :: WK
 
!     Some checks 
 
IFAULT = 0
if (NREQ > NCOL) then 
  IFAULT = IFAULT + 4 
end if
if (IFAULT /= 0) then 
  return 
end if 
 
!     The elements of xrow.inv(R).sqrt(D) are calculated and stored
!     in WK.
 
HII = ZERO
do COL = 1, NREQ 
  if (sqrt(D(COL)) <= TOL(COL)) then
    WK(COL) = ZERO 
  else
    POS = COL - 1  
    TOTAL = XROW(COL)
    do ROW = 1, COL-1  
      TOTAL = TOTAL - WK(ROW)*R(POS)
      POS = POS + NCOL - ROW - 1
    end do ! row = 1, col-1
    WK(COL) = TOTAL
    HII = HII + TOTAL**2 / D(COL) 
  end if 
end do ! col = 1, nreq 
 
return
end subroutine HDIAG 
 
subroutine VARPRD(X, NREQ, FN_VAL) 
!
!  Author: Allen Miller
!
integer, intent(in)       :: NREQ 
real (kind=DP), dimension(:), intent(in)  :: X
real (kind=DP), intent(out)   :: FN_VAL
 
!     Local variables
 
integer         :: IFAULT, ROW
real (kind=DP)  :: VAR  
real (kind=DP), dimension(NREQ) :: WK 
 
!     Check input parameter values
 
FN_VAL = ZERO
IFAULT = 0
if (NREQ < 1 .or. NREQ > NCOL) then
  IFAULT = IFAULT + 4
end if
if (NOBS <= NREQ) then 
  IFAULT = IFAULT + 8  
end if 
if (IFAULT /= 0) then
  write(unit=*, fmt='(1x, a, i4)') 'Error in function VARPRD: ifault =', IFAULT 
  return
end if
 
!     Calculate the residual variance estimate.
 
VAR = SSERR / (NOBS - NREQ) 
 
call BKSUB2(X, WK, NREQ)
do ROW = 1, NREQ 
  if(D(ROW) > TOL(ROW)) then 
    FN_VAL = FN_VAL + WK(ROW)**2 / D(ROW) 
  end if 
end do 
 
FN_VAL = FN_VAL * VAR  
 
return
end subroutine VARPRD 
 
subroutine BKSUB2(X, B, NREQ)
!
!  Author: Allen Miller
!
integer, intent(in):: NREQ 
real (kind=DP), dimension(:), intent(in)   :: X
real (kind=DP), dimension(:), intent(out)  :: B
integer :: POS, ROW, COL 
real (kind=DP)   :: TEMP 
do ROW = 1, NREQ 
  POS = ROW - 1
  TEMP = X(ROW) 
  do COL = 1, ROW-1 
    TEMP = TEMP - R(POS)*B(COL)
    POS = POS + NCOL - COL - 1
  end do
  B(ROW) = TEMP 
end do
return
end subroutine BKSUB2
 
end module LSQ
 
subroutine polyfit(iorder,nsize,xr4,yr4,rcoeff,rsterr,r2)
!
!  Source extensively modified from: http://jblevins.org/mirror/amiller/
!  Purpose: Fit a polynomial fuction of form:  y = a(0) + a(1).x + a(2).x^2 + ... + a(m).x^m
!           Max allowed order is 20 - 1 = 19.  This may be changed by changing array structure
!
!    Author: Kenny Anderson
!    Created: May 2010
!
  use lsq
  implicit none
  integer, intent(in) :: iorder !the order of the polynomial
  integer, intent(in) :: nsize  !number of elements in array
  real, dimension(nsize), intent(in)  :: xr4  !array of dependent data single precision
  real, dimension(nsize), intent(in)  :: yr4  !array of independent data single precision
  double precision, dimension(iorder+1), intent(out) :: rcoeff
  real, dimension(iorder+1), intent(out) :: rsterr
  real, intent(out) :: r2
  real(dp), dimension(nsize) :: x 
  real(dp), dimension(nsize) :: y 
  real(dp) :: wt=1.0_dp 
  real(dp) :: beta(0:20), xrow(0:20), sterr(0:20)
  real(dp) :: var,  totalss
  real(dp), dimension(231) :: covmat
  integer :: i, ier, iostatus, j, n
  logical :: lfit_const
  logical ::  lindep(0:iorder)
  integer :: icnt
 
  wt=1.0
  lfit_const = .true.
  x(1:nsize) = xr4(1:nsize)
  y(1:nsize) = yr4(1:nsize)
!
! least-squares calculations
!
  call startup(iorder, lfit_const)
  do i = 1, nsize
    xrow(0) = 1.0
    do j = 1, iorder
    xrow(j) = x(i) * xrow(j-1)
    end do
    call includ(wt, xrow, y(i))
  end do
 
  call sing(lindep, ier)
  if (ier /= 0) then
    do i = 0, iorder
    if (lindep(i)) write(*, '(a, i3)') ' singularity detected for power: ', i
    end do
  end if
! calculate progressive residual sums of squares
  call ss()
  var = rss(iorder+1) / (nsize - iorder - 1)
! calculate least-squares regn. coeffs.
  call regcf(beta, iorder+1, ier)
! calculate covariance matrix, and hence std. errors of coeffs.
  call cov(iorder+1, var, covmat, 231, sterr, ier)
  do i =0,iorder
     rcoeff(i+1)=beta(i)
     rsterr(i+1)=sterr(i)
  end do
  totalss = rss(1)
  r2=(totalss - rss(iorder+1))/totalss
 
  return
end subroutine polyfit
 
subroutine expfit(nsize,xr4,yr4,rcoeff,rsterr,r2)
!
!  Source extensively modified from: http://jblevins.org/mirror/amiller/
!  Purpose: Fit an exponential fuction of form:  A*exp(B*x)
!           This equation is linearized into ln(y)=ln(A)+B*x
!
!    Author: Kenny Anderson
!    Created: May 2010
!
  use lsq
  implicit none
  integer, parameter :: iorder = 1
  integer, intent(in) :: nsize  !number of elements in array
  real, dimension(nsize), intent(in)  :: xr4  !array of dependent data single precision
  real, dimension(nsize), intent(in)  :: yr4  !array of independent data single precision
  double precision, dimension(iorder+1), intent(out) :: rcoeff
  real, dimension(iorder+1), intent(out) :: rsterr
  real, intent(out) :: r2
  real(dp), dimension(nsize) :: x
  real(dp), dimension(nsize) :: y
  real(dp) :: wt=1.0_dp
  real(dp) :: beta(0:iorder), xrow(0:iorder), sterr(0:iorder)
  real(dp) :: var,  totalss
  real(dp), dimension(231) :: covmat
  integer :: i, ier, iostatus, j, n
  logical :: lfit_const
  logical ::  lindep(0:iorder)
  integer :: icnt
  real :: rmax
  real :: rmin

  ! PHJ check
  !do i=1, nsize
    !print*, xr4(i), yr4(i)
  !end do
   
  wt=1.0
  lfit_const = .true.
  rmin = minval(yr4, mask = yr4 .ne. 0.000000000000000000000000)
  rmax = maxval(yr4, mask = yr4 .ne. 0.000000000000000000000000)
  do i=1,nsize
    if(yr4(i) .eq. 0.000000000000000000000000) then
      if(yr4(nsize) .lt. yr4(1)) then
        y(i) = rmin
      else
        y(i) = rmax
      end if
    else
      y(i) = yr4(i)
    end if
  end do
  x(1:nsize) = xr4(1:nsize)
!
! least-squares calculations
!
  call startup(iorder, lfit_const)
  do i = 1, nsize
    y(i)=log(y(i))
    xrow(0) = 1.0
    xrow(1) = x(i)
    call includ(wt, xrow, y(i))
  end do
  call sing(lindep, ier)
  if (ier /= 0) then
    do i = 0, iorder
  if (lindep(i)) write(*, '(a, i3)') ' singularity detected for power: ', i
    end do
  end if
! calculate progressive residual sums of squares
  call ss()
  var = rss(iorder+1) / (nsize - iorder - 1)
! calculate least-squares regn. coeffs.
  call regcf(beta, iorder+1, ier)
! calculate covariance matrix, and hence std. errors of coeffs.
  call cov(iorder+1, var, covmat, 231, sterr, ier)
! finalize answer
  rcoeff(1)=exp(beta(0))
  rcoeff(2)=beta(1)
  rsterr(1)=exp(sterr(0))
  rsterr(2)=sterr(1)
  totalss = rss(1)
  r2=(totalss - rss(iorder+1))/totalss
!
  return
end subroutine expfit
 
subroutine powerfit(nsize,xr4,yr4,rcoeff,rsterr,r2)
!
!  Source extensively modified from: http://jblevins.org/mirror/amiller/
!  Purpose: Fit a power law fuction of form:  y = A*x^B
!           This equation is linearized into ln(y)=ln(A)+B*ln(x)
!
!    Author: Kenny Anderson
!    Created: May 2010
!
  use lsq
  implicit none
  integer, parameter :: iorder = 1
  integer, intent(in) :: nsize  !number of elements in array
  real, dimension(nsize), intent(in)  :: xr4  !array of dependent data single precision
  real, dimension(nsize), intent(in)  :: yr4  !array of independent data single precision
  double precision, dimension(iorder+1), intent(out) :: rcoeff
  real, dimension(iorder+1), intent(out) :: rsterr
  real, intent(out) :: r2
  real(dp), dimension(nsize) :: x
  real(dp), dimension(nsize) :: y
  real(dp) :: wt=1.0_dp
  real(dp) :: beta(0:iorder), xrow(0:iorder), sterr(0:iorder)
  real(dp) :: var,  totalss
  real(dp), dimension(231) :: covmat
  integer :: i, ier, iostatus, j, n
  logical :: lfit_const
  logical ::  lindep(0:iorder)
  integer :: icnt
  real :: rmax
  real :: rmin
  
  wt=1.0
  lfit_const = .true.
  rmin = minval(yr4, mask = yr4 .ne. 0.000000000000000000000000)
  rmax = maxval(yr4, mask = yr4 .ne. 0.000000000000000000000000)
  do i=1,nsize
    if(yr4(i) .eq. 0.000000000000000000000000) then
      if(yr4(nsize) .lt. yr4(1)) then
        y(i) = rmin
      else
        y(i) = rmax
      end if
    else
      y(i) = yr4(i)
    end if
  end do
  x(1:nsize) = xr4(1:nsize)
!
! least-squares calculations
!
  call startup(iorder, lfit_const)
  do i = 1, nsize
    y(i)=log(y(i))
    xrow(0) = 1.0
    xrow(1) = log(x(i))
    call includ(wt, xrow, y(i))
  end do
  call sing(lindep, ier)
  if (ier /= 0) then
    do i = 0, iorder
  if (lindep(i)) write(*, '(a, i3)') ' singularity detected for power: ', i
    end do
  end if
! calculate progressive residual sums of squares
  call ss()
  var = rss(iorder+1) / (nsize - iorder - 1)
! calculate least-squares regn. coeffs.
  call regcf(beta, iorder+1, ier)
! calculate covariance matrix, and hence std. errors of coeffs.
  call cov(iorder+1, var, covmat, 231, sterr, ier)
! finalize answer
  rcoeff(1)=exp(beta(0))
  rcoeff(2)=beta(1)
  rsterr(1)=exp(sterr(0))
  rsterr(2)=sterr(1)
  totalss = rss(1)
  r2=(totalss - rss(iorder+1))/totalss
!
  return
end subroutine powerfit
 
subroutine powerfit_fixed(nsize,xr4,yr4,rpower,rcoeff,rsterr,r2)
!
!  Source extensively modified from: http://jblevins.org/mirror/amiller/
!  Purpose: Fit a fixed exponent power law fuction of form:  y = A*x^B
!           The power (B) is specified by user input, in variable rpower
!
!    Author: Kenny Anderson
!    Created: May 2010
!
  use lsq
  implicit none
  integer, parameter :: iorder = 0
  integer, intent(in) :: nsize  !number of elements in array
  real, dimension(nsize), intent(in)  :: xr4  !array of dependent data single precision
  real, dimension(nsize), intent(in)  :: yr4  !array of independent data single precision
  real, intent(in) :: rpower
  double precision, dimension(iorder+1), intent(out) :: rcoeff
  real, dimension(iorder+1), intent(out) :: rsterr
  real, intent(out) :: r2
  real(dp), dimension(nsize) :: x
  real(dp), dimension(nsize) :: y
  real(dp) :: wt=1.0_dp
  real(dp) :: beta(0:iorder), xrow(0:iorder), sterr(0:iorder)
  real(dp) :: var,  totalss
  real(dp), dimension(231) :: covmat
  integer :: i, ier, iostatus, j, n
  logical :: lfit_const
  logical ::  lindep(0:iorder)
  integer :: icnt
  real(dp), dimension(nsize) :: f, ss_tot, ss_err
  real(dp) :: ya
  real :: rmax
  real :: rmin
 
  wt=1.0
  lfit_const = .true.
  rmin = minval(yr4, mask = yr4 .ne. 0.000000000000000000000000)
  rmax = maxval(yr4, mask = yr4 .ne. 0.000000000000000000000000)
  do i=1,nsize
    if(yr4(i) .eq. 0.000000000000000000000000) then
      if(yr4(nsize) .lt. yr4(1)) then
        y(i) = rmin
      else
        y(i) = rmax
      end if
    else
      y(i) = yr4(i)
    end if
  end do
  x(1:nsize) = xr4(1:nsize)
!
! least-squares calculations
!
  call startup(iorder, lfit_const)
  do i = 1, nsize
    xrow(0) = x(i)**rpower
    call includ(wt, xrow, y(i))
  end do
  call sing(lindep, ier)
  if (ier /= 0) then
    do i = 0, iorder
  if (lindep(i)) write(*, '(a, i3)') ' singularity detected for power: ', i
    end do
  end if
! calculate progressive residual sums of squares
  call ss()
  var = rss(iorder+1) / (nsize - iorder - 1)
! calculate least-squares regn. coeffs.
  call regcf(beta, iorder+1, ier)
! calculate covariance matrix, and hence std. errors of coeffs.
  call cov(iorder+1, var, covmat, 231, sterr, ier)
! finalize answer
  rcoeff(1)=beta(0)
  rsterr(1)=sterr(0)
  ya=sum(y)/real(nsize)
  do i=1,nsize
    f(i)=rcoeff(1)*x(i)**rpower
    ss_err(i)=(y(i)-f(i))**2
    ss_tot(i)=(y(i)-ya)**2
  end do
  r2=1.0-sum(ss_err)/sum(ss_tot)
!
  return
end subroutine powerfit_fixed 
 
subroutine logfit(nsize,xr4,yr4,rcoeff,rsterr,r2)
!
!  Source extensively modified from: http://jblevins.org/mirror/amiller/
!  Purpose: Fit a power law fuction of form:  y = A + B*ln(x)
!
!    Author: Kenny Anderson
!    Created: May 2010
!
  use lsq
  implicit none
  integer, parameter :: iorder = 1
  integer, intent(in) :: nsize  !number of elements in array
  real, dimension(nsize), intent(in)  :: xr4  !array of dependent data single precision
  real, dimension(nsize), intent(in)  :: yr4  !array of independent data single precision
  double precision, dimension(iorder+1), intent(out) :: rcoeff
  real, dimension(iorder+1), intent(out) :: rsterr
  real, intent(out) :: r2
  real(dp), dimension(nsize) :: x
  real(dp), dimension(nsize) :: y
  real(dp) :: wt=1.0_dp
  real(dp) :: beta(0:iorder), xrow(0:iorder), sterr(0:iorder)
  real(dp) :: var,  totalss
  real(dp), dimension(231) :: covmat
  integer :: i, ier, iostatus, j, n
  logical :: lfit_const
  logical ::  lindep(0:iorder)
  integer :: icnt
  wt=1.0
  lfit_const = .true.
  x(1:nsize) = xr4(1:nsize)
  y(1:nsize) = yr4(1:nsize)
!
! least-squares calculations
!
  call startup(iorder, lfit_const)
  do i = 1, nsize
    xrow(0) = 1.0
    xrow(1) = log(x(i))
    call includ(wt, xrow, y(i))
  end do
  call sing(lindep, ier)
  if (ier /= 0) then
    do i = 0, iorder
      if (lindep(i)) write(*, '(a, i3)') ' singularity detected for power: ', i
    end do
  end if
! calculate progressive residual sums of squares
  call ss()
  var = rss(iorder+1) / (nsize - iorder - 1)
! calculate least-squares regn. coeffs.
  call regcf(beta, iorder+1, ier)
! calculate covariance matrix, and hence std. errors of coeffs.
  call cov(iorder+1, var, covmat, 231, sterr, ier)
! finalize answer
  rcoeff(1)=beta(0)
  rcoeff(2)=beta(1)
  rsterr(1)=sterr(0)
  rsterr(2)=sterr(1)
  totalss = rss(1)
  r2=(totalss - rss(iorder+1))/totalss
  return
end subroutine logfit
subroutine quad_nocons_fit(nsize,xr4,yr4,rcoeff,rsterr,r2)
!
!  Source extensively modified from: http://jblevins.org/mirror/amiller/
!  Purpose: Fit a polynomial of form: y = a(0).x +  a(1).x^2
!
!    Author: Kenny Anderson
!    Created: May 2010
!
  use lsq
  implicit none
  integer, parameter :: iorder = 1 !the order of the polynomial
  integer, intent(in) :: nsize  !number of elements in array
  real, dimension(nsize), intent(in)  :: xr4  !array of dependent data single precision
  real, dimension(nsize), intent(in)  :: yr4  !array of independent data single precision
  double precision, dimension(iorder+1), intent(out) :: rcoeff
  real, dimension(iorder+1), intent(out) :: rsterr
  real, intent(out) :: r2
  real(dp), dimension(nsize) :: x
  real(dp), dimension(nsize) :: y
  real(dp) :: wt=1.0_dp
  real(dp) :: beta(0:(iorder+1)), xrow(0:(iorder+1)), sterr(0:(iorder+1))
  real(dp) :: var,  totalss
  real(dp), dimension(231) :: covmat
  integer :: i, ier, iostatus, j, n
  logical :: lfit_const
  logical ::  lindep(0:(iorder+1))
  integer :: icnt
  
  wt=1.0
  lfit_const = .true.
  x(1:nsize) = xr4(1:nsize)
  y(1:nsize) = yr4(1:nsize)
!
! least-squares calculations
!
  call startup(iorder, lfit_const)
  do i = 1, nsize
    xrow(0) = x(i)
    xrow(1) = x(i)**2
    call includ(wt, xrow, y(i))
  end do
  
  call sing(lindep, ier)
  if (ier /= 0) then
    do i = 0, iorder
      if (lindep(i)) write(*, '(a, i3)') ' singularity detected for power: ', i
    end do
  end if
! calculate progressive residual sums of squares
  call ss()
  var = rss(iorder+1) / (nsize - iorder - 1)
! calculate least-squares regn. coeffs.
  call regcf(beta, iorder+1, ier)
! calculate covariance matrix, and hence std. errors of coeffs.
  call cov(iorder+1, var, covmat, 231, sterr, ier)
! finalize answer
  do i =0,iorder
     rcoeff(i+1)=beta(i)
     rsterr(i+1)=sterr(i)
  end do
  totalss = rss(1)
  r2=(totalss - rss(iorder+1))/totalss
 
  return
end subroutine quad_nocons_fit
