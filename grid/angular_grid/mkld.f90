program main

!*****************************************************************************80
!
!  Usage: run with ./mkld n, where n is the order of the quadrature
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!
!  Originally by John something
!
!  Modified:
!
!    23 May 2019
!
!  Author:
!
!    Philip
!
  implicit none

  call test02 ( )

  stop
end
subroutine test02 ( )

!*****************************************************************************80
!
!c TEST02 tests the SPHERE_LEBEDEV_RULE functions.
!
!  Modified:
!
!    13 September 2010
!
!  Author:
!
!    Dmitri Laikov
!
!  Reference:
!
!    Vyacheslav Lebedev, Dmitri Laikov,
!    A quadrature formula for the sphere of the 131st
!    algebraic order of accuracy,
!    Russian Academy of Sciences Doklady Mathematics,
!    Volume 59, Number 3, 1999, pages 477-481.
!
  implicit none

  integer ( kind = 4 ), parameter :: nmax = 65
  integer ( kind = 4 ), parameter :: mmax = &
    ( ( nmax * 2 + 3 ) * ( nmax * 2 + 3 ) / 3 )

  real ( kind = 8 ) alpha
  integer ( kind = 4 ) available
  integer ( kind = 4 ) available_table
  real ( kind = 8 ) beta
  real ( kind = 8 ) err
  real ( kind = 8 ) err_max
  integer ( kind = 4 ) i
  real ( kind = 8 ) integral_exact
  real ( kind = 8 ) integral_approx
  integer ( kind = 4 ) j
  integer ( kind = 4 ) k
  integer ( kind = 4 ) m
  integer ( kind = 4 ) n
  integer ( kind = 4 ) order
  integer ( kind = 4 ) order_table
  integer ( kind = 4 ) precision_table
  real ( kind = 8 ) s(0:nmax+1)
  real ( kind = 8 ) w(mmax)
  real ( kind = 8 ) x(mmax)
  real ( kind = 8 ) xn(mmax,0:nmax)
  real ( kind = 8 ) y(mmax)
  real ( kind = 8 ) yn(mmax,0:nmax)
  real ( kind = 8 ) z(mmax)
  real ( kind = 8 ) zn(mmax,0:nmax)
  real ( kind = 8 ) t(mmax)
  real ( kind = 8 ) p(mmax)
  character(100) :: arg

  character(100) :: filename

  ! get command line argument
  call get_command_argument(1,arg)

  read(arg,*) n 

  order = order_table ( n )

  write(filename, *) order

  filename = adjustl(filename)

  call ld_by_order ( order, x, y, z, w )

  open(unit=15, file=filename)

  write(15, *) order
  do m = 1, order
      !call xyz_to_tp(x(m), y(m), z(m), t(m), p(m))
      !write (15, '(3E24.16)') t(m), p(m), w(m)
      write (15, '(4E24.16)') x(m), y(m), z(m), w(m)
  end do
  close(unit=15)

  return
end

