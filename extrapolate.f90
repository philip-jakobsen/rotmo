!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to perform extrapolation of rotation angles by an exponential fit
!
subroutine fit()

    use common_variables

    use orbital_variables

    use sweep_variables

    use fit_variables

    integer :: iocc
    integer :: ivirt

    real*8 :: a, b, rsq

    real*8 :: before_extrapolation
    real*8 :: after_extrapolation

    ! make x array
    allocate(hist_x(ex_end-ex_start))
    hist_x = 0.0d0

    do i=1, ex_end-ex_start
        
        hist_x(i) = dble(i+ex_start-1)

    end do

    print*, "================================================================"
    print "(A30)", "extrapolate talking"
    print "(A30,A3)", "Type: ", ex_type
    print "(A30,I3,x,I3)", "Range of data points: ", ex_start, ex_end
    print "(A30,f8.3)", "R^2 threshold = ", ex_tol
    print*, "Number of iterations to extrapolate to = ", ex_num
    print*, "================================================================"

    ! checking
    call evalUdens2(dtol, .false., 0.0d0, 1, 2)
    call calcrmsd(udens, refaodens, .false.)
    deallocate(udens)
    before_extrapolation = rmsd_value

    ! hold a, b and R^2
    allocate(paramlist(wnnprim,nocc,3))

    ! hold extrapolated values
    allocate(extrapolated_alphas(wnnprim,wnnprim))
    extrapolated_alphas = 0.0d0

    ! perform fit 
    do iocc=1, nocc

        do ivirt=nocc+1, wnnprim

            ! if symmetry check, only consider pairs with same symmetry
            if (symcheck .eqv. .true. ) then
                
                if (symlabels(iocc) .ne. symlabels(ivirt)) then

                    print "(A20, 2I4, A20)", "Skipping pair", iocc, ivirt, "due to symmetry"

                    cycle

                end if

            end if

            ! check if first alpha is negative. If yes, flip sign of whole
            ! array
            if (hist_alphas(iocc,ivirt,ex_start) .lt. 0.0d0) then

                print*, "pair contains negative angles, flipping sign"

                hist_alphas(iocc,ivirt,:) = abs(hist_alphas(iocc,ivirt,:))

            end if

            allocate(fit_rcoeff(2))
            allocate(fit_rsterr(2))

            if (ex_type .eq. "EXP") then

                call expfit(ex_end-ex_start, hist_x(:),  &
                    & hist_alphas(iocc,ivirt,ex_start:ex_end),fit_rcoeff,fit_rsterr,fit_rsquared)

            else if (ex_type .eq. "POW") then

                call powerfit(ex_end-ex_start, hist_x(:),  &
                    & hist_alphas(iocc,ivirt,ex_start:ex_end),fit_rcoeff,fit_rsterr,fit_rsquared)

            else if (ex_type .eq. "LOG") then

                call logfit(ex_end-ex_start, hist_x(:),  &
                    & hist_alphas(iocc,ivirt,ex_start:ex_end),fit_rcoeff,fit_rsterr,fit_rsquared)

            else if (ex_type .eq. "LIN") then

                call polyfit(1,ex_end-ex_start, hist_x(:),  &
                    & hist_alphas(iocc,ivirt,ex_start:ex_end),fit_rcoeff,fit_rsterr,fit_rsquared)

            end if

            print*, "pair", iocc, ivirt
            print*, "A = ", fit_rcoeff(1)
            print*, "B = ", fit_rcoeff(2)
            print*, "R^2 = ", fit_rsquared
            print*, ""

            paramlist(ivirt,iocc,1) = fit_rcoeff(1)
            paramlist(ivirt,iocc,2) = fit_rcoeff(2)
            paramlist(ivirt,iocc,3) = fit_rsquared

            a = paramlist(ivirt,iocc,1)
            b = paramlist(ivirt,iocc,2)
            rsq = paramlist(ivirt,iocc,3)


            ! perform extrapolation
            if (rsq .ge. ex_tol) then

                print*, "Performing extrapolation"

                ! case finite extrapolation
                if (ex_num .lt. 200 ) then

                    print*, "Using finite sum, n = ", ex_num

                    ! OLD version
                    !! extrapolating to ex_num iterations
                    !do i=1, ex_num

                        !tmp = tmp + (a*exp(b*i))

                    !end do

                    !extrapolated_alphas(iocc,ivirt) = tmp

                    ! extrapolate from i to n,, aka Sum[a+Exp(-b*x),{x,i,n}] =
                    ! (a(exp(b*i)-exp(b(1+n)))/(-1+exp(b)
                    extrapolated_alphas(iocc,ivirt) = -(a*(exp(b*dble(ex_start))-exp(b*(1+dble(ex_num)))))/(-1.0d0+exp(b))

                else 

                    print*, "Using infinite sum"

                    extrapolated_alphas(iocc,ivirt) = -(a*exp(b))/(-1.0d0+exp(b))

                end if

            end if

            print "(A30,2I5,A10,E24.16)", "Updating pair ", iocc, ivirt, "alpha =", extrapolated_alphas(iocc,ivirt)

            ! update UMO
            ! arguments: <occ MO index ii>, < virtual MO index j>, <list>
            ! output: "updated UMO"
            call update_umos(iocc, ivirt, extrapolated_alphas(iocc, ivirt))
            


            deallocate(fit_rcoeff)
            deallocate(fit_rsterr)

        end do ! ivirt

    end do ! iocc

    print*, "Extrapolation complete"
    print*, ""

    ! checking
    call evalUdens2(dtol, .false., 0.0d0, 1, 2)
    call calcrmsd(udens, refaodens, .false.)
    deallocate(udens)

    after_extrapolation = rmsd_value

    print "(A30, E24.16)", "MAD before extrapolation = ", before_extrapolation
    print "(A30, E24.16)", "MAD after extrapolation = ", after_extrapolation


    if (after_extrapolation .gt. before_extrapolation) then

        print*, "Extrapolation gave higher MAD, exiting, reverting extrapolation"

        do iocc=1, nocc

            do ivirt=1+nocc, wnnprim

                call update_umos(iocc, ivirt, -extrapolated_alphas(iocc, ivirt))

            end do

        end do

    end if


end subroutine
