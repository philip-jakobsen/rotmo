!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
! subroutine to minimize density differences by pseudo-jacobi sweep
! algorithm
subroutine jacobisweep(iter,inwrite, indebug)

    use orbital_variables

    use grid_variables

    use common_variables

    use sweep_variables

    implicit none

    logical                     :: inwrite
    logical                     :: indebug
    integer                     :: iter

    real*8                      :: alpha_sweep

    integer                     :: i

    integer                     ::  iocc, ivirt, microiter

    real*8                      :: micro_current_err
    real*8                      :: micro_previous_err
    real*8                      :: initial_err 
    real*8                      :: macro_err 
    real*8                      :: best_err

    ! to hold best alpha and MAD for each rotated MO
    real*8, dimension(:), allocatable :: alphas, best

    ! bisect
    real*8                      :: x1_alpha, x2_alpha, mid_alpha
    real*8                      :: x1_err, x2_err, mid_err

    real*8                      :: jacobisweep_time_start
    real*8                      :: jacobisweep_time_finish

    real*8 :: alpha_start

    real*8 :: modiff

    real*8 :: sgn


    !print "(A30)", "Starting time of jacobi sweep"
    call cpu_time(jacobisweep_time_start)

    ! checking if UMO is allocated
    if (allocated(UMO) .eqv. .false.) then
        print "(A30)", "Allocating UMO"

        allocate(UMO(wnnprim, wnnprim))

        ! copying MO to UMO
        UMO = MO

    end if

    ! find density with no rotation among orbitals
    call evalUdens2(dtol, .false., 0.0d0, 1, 2)

    call calcerrf(udens, refaodens, .true.)
    !call calcmad(udens, refaodens, .true.)
    print "(A30)", "(what we want to beat)"
    
    deallocate(udens)

    ! initial, no rotation mad (what we want to beat)
    !initial_err = mad_value
    initial_err = errf_value

    if (initial_err .lt. sweeptol) then

        print "(A30,E24.16,A5,E24.16)", "Initial_mad = ", initial_err, "< ", sweeptol
        print "(A30)", "exiting"

        return

    end if

    ! initial conditions
    macro_err = initial_err

    micro_current_err = initial_err

    micro_previous_err = initial_err

    best_sweep_err = initial_err
    previous_best_sweep_err = initial_err


    alpha_start = 0.0d0

    ! allocating array to hold optimal alphas
    allocate(alphas(wnnprim))
    allocate(best(wnnprim))

    alphas = 0.0d0
    best = 0.0d0


    best(1) = initial_err

    if (allocated(udens_copy) .eqv. .true.) then

        deallocate(udens_copy)

    end if

    allocate(udens_copy(npoints))

    print*, "*"
    print*, "*"
    print*, "*"
    print*, "================================================================"
    print "(A30)", "sweep talking"
    print "(A30, I3)", "occ = ", nocc
    print "(A30, f10.6)", "step size = ", abs(alpha_step)
    print "(A30, I8)", "maxiter = ", maxlineiter
    print "(A30, L)", "iterprint = ", print_iter
    print "(A30, L)", "update UMO = ", update_umo
    print "(A30, L)", "check ortho = ", ortho_check
    print "(A30, I8)", "maxbisectiter= ", maxbisectiter
    print "(A30, E10.4)", "bisect tol (mad)= ", bisecttol
    print*, "================================================================"
    print*, "*"
    print*, "*"
    print*, "*"

    ! occupied MO loop
    do iocc=1, nocc
        
        ! resetting these for each occ mo
        alphas = 0.0d0
        best = 0.0d0

        ! virtual MO loop
        do ivirt=nocc+1, wnnprim
        !do ivirt=wnnprim, nocc+1, -1
        
            ! check for same symmetry
            if (symcheck .eqv. .true.) then

                if (symlabels(iocc) .ne. symlabels(ivirt)) then

                    !if (print_iter .eqv. .true.) 
                    print "(A20, 2I4, A20)", "Skipping pair", iocc, ivirt, "due to symmetry"

                    ! save best mad equal to previous
                    best(ivirt) = best(ivirt-1)

                    ! set to zero
                    alphas(ivirt) = 0.0d0

                    cycle

                end if

            end if

            print*, ""
            print*, "***"
            print*, "=============================================="
            print "(A20, 2I4, A20, A20)", "Rotating pair", &
                & iocc, ivirt, "with virt symmetry", symlabels(ivirt) 
            print*, "***"
            print*, ""

            ! reset bisect 
            mid_alpha = 0.0d0
            mid_err = 0.0d0

            ! reset alpha
            alpha_sweep = 0.0d0

            !macro_err = current_mad
            best_err = macro_err

            micro_current_err = best_err
            micro_previous_err = best_err

            
            ! starting with positive alphas
            sgn = 1.0d0

            alpha_step = abs(alpha_step)

            if (print_iter .eqv. .true.) print "(A20, E10.4)", "Starting at alpha = ", alpha_sweep

  ! this is bad, I know, I know
  900       do microiter=1, maxlineiter

                ! update with sign information
                if (microiter .eq. 1) then
                    
                    alpha_step = alpha_step*sgn

                end if

                alpha_sweep = alpha_sweep + alpha_step

                ! no write to file for each iteration
                call evalUdens2(dtol, .false., alpha_sweep, iocc, ivirt)

                ! if we want to check number of electrons
                call sumdens(udens, print_iter)

                call calcerrf(udens, refaodens, .false.)

                deallocate(udens)

                ! update previous
                micro_previous_err = micro_current_err

                ! update current with new MAD from calcmad
                micro_current_err = errf_value

                if (print_iter .eqv. .true. ) then

                    print*, "====================="
                    print*, "Iteration information"
                    print "(A30, I3)", "iocc = ", iocc
                    print "(A30, I4)", "ivirt = ", ivirt
                    print "(A30, I8)", "microiter = ", microiter
                    print "(A30, E24.16)", "Step = ", alpha_step
                    print "(A30, E24.16)", "Alpha = ", alpha_sweep
                    print "(A30, E24.16)", "Previous MAD = ", micro_previous_err
                    print "(A30, E24.16)", "Current MAD = ", micro_current_err
                    print "(A30, E24.16)", "Best MAD = ", best_err
                    print*, "====================="

                end if

                ! write walk to file
                !if (inwrite .eqv. .true.) then

                    !write(ivirt + 20, "(2E24.16)") alpha_sweep, micro_current_err

                !end if

                ! convergence criteria
                !
                ! not converged, go on
                if (micro_current_err .lt. micro_previous_err) then

                    continue

                ! negative change
                ! setting new macro_err to previous (minima)
                else

                    !if (print_iter .eqv. .true) then

                        if (print_iter .eqv. .true.) print*, "Current higher than previous"
                        print*, "Switching to bisect after ", microiter, "line search iterations"

                    !end if

                    ! initial interval for alpha
                    ! current (which is one too far) and 2x previous for general
                    ! case, 
                    ! current and previous for case microiter = 1
                    if (microiter .eq. 1 ) then

                        !x1_alpha = alpha_sweep - alpha_step
                        x1_alpha = 0.0d0

                        micro_previous_err = best_err

                    else

                        x1_alpha = alpha_sweep - 2*alpha_step

                    end if

                    x2_alpha = alpha_sweep

                    ! do bisecting
                    do i=1, maxbisectiter

                        mid_alpha = (x1_alpha + x2_alpha) / 2.0d0

                        ! x1
                        call evalUdens2(dtol, .false., x1_alpha, iocc, ivirt)
                        call calcerrf(udens, refaodens, .false.)
                        deallocate(udens)
                        x1_err = errf_value

                        ! mid
                        call evalUdens2(dtol, .false., mid_alpha, iocc, ivirt)
                        call calcerrf(udens, refaodens, .false.)
                        deallocate(udens)
                        mid_err = errf_value

                        ! x2
                        call evalUdens2(dtol, .false., x2_alpha, iocc, ivirt)
                        call calcerrf(udens, refaodens, .false.)
                        deallocate(udens)
                        x2_err = errf_value

                        ! write walk to file
                        !if (inwrite .eqv. .true.) then

                        !write(ivirt + 20, "(2E24.16)") mid_alpha, mid_err

                        !end if

                        if (print_iter .eqv. .true.) then

                            print "(A30, I4)", "Bisect iter = ", i
                            print "(A30, E24.16)", "x1_alpha = ", x1_alpha
                            print "(A30, E24.16)", "mid_alpha = ", mid_alpha
                            print "(A30, E24.16)", "x2_alpha = ", x2_alpha
                            print "(A30, E24.16)", "x1_err = ", x1_err
                            print "(A30, E24.16)", "mid_err = ", mid_err
                            print "(A30, E24.16)", "x2_err = ", x2_err

                        end if

                        ! updating mid
                        ! mid_err will always be lower than x1_err or x2_err
                        if ( (x1_err - mid_err) .lt. (x2_err - mid_err) ) then

                            ! left side
                            x2_alpha = mid_alpha

                        else

                            ! right side
                            x1_alpha = mid_alpha

                        end if

                        ! convergence criteria for bisect
                        if (abs(x1_err - x2_err ) .lt. bisecttol) then

                            if (print_iter .eqv. .true.) print "(A30)", "Bisect converged"
                            if (print_iter .eqv. .true.) print "(A30,E10.4)", "abs(x1_err - x2_err) < ", bisecttol

                            ! if converged very close to 0, discard
                            if (mid_err > micro_previous_err) then

                                print "(A50)", "Nothing here, mid_err > micro_previous_err"

                                if (sgn .eq. 1.0d0) then

                                    print "(A30)", "******"
                                    print "(A30)", "Trying negative angles"
                                    print "(A30)", "******"

                                    sgn = -1.0d0
                                    alpha_sweep = 0.0d0
                                    goto 900

                                end if

                                alpha_sweep = 0.0d0
                                micro_current_err = best_err
                                mid_err = macro_err

                                exit

                            else

                                print "(A30,I2,A30)", "Minima found in ", i, " bisect iterations"

                                alpha_sweep = mid_alpha

                                best_err = mid_err

                                ! for convergence testing
                                write(ivirt+20, "(I4,E24.16)") iter, mid_alpha
                                hist_alphas(iocc,ivirt,iter) = mid_alpha

                                exit

                            end if

                        end if

                        ! too many iterations
                        if (i .eq. maxbisectiter) then

                            print "(A30)", "Max bisect iter reached :("

                        end if

                    end do ! bisect iter

                    ! call again to get optimal udens
                    call evalUdens2(dtol, .false., mid_alpha, iocc, ivirt)

                    udens_copy = udens

                    deallocate(udens)

                    
                    ! if we want to update the UMO
                    if (update_umo .eqv. .true.) then

                        ! from bisect
                        best_err = mid_err

                        ! set current to best
                        macro_err = best_err

                    else

                        micro_current_err = initial_err

                    end if

                    ! save best mad
                    best(ivirt) = best_err

                    ! save optimal alpha
                    alphas(ivirt) = alpha_sweep
                    
                    exit

                end if

            end do

            print*, ""

            print "(A30, E24.16)", "alpha = ", alpha_sweep
            print "(A30, E24.16)", "Best MAD = ", best_err
            print*, ""

            ! to check number of electrons are correct
            !call evalUdens2(innpoints, innnprim, 1.0d-10, .false., .false., alpha_sweep, ivirt+innocc)
            !call sumdens(innpoints, udens, grid)
            !deallocate(udens)

            if (update_umo .eqv. .true.) then

                print*, "Updated UMO"

                ! update CMO with optimal alpha
                call update_umos(iocc, ivirt, alpha_sweep)

                if (print_iter .eqv. .true.) then
                    print "(A40,E24.16)", "Updated UMO based on alpha = ", alpha_sweep
                    print*, "=============================================="
                    print*, ""
                    print*, ""
                    print*, ""
                end if

                if (ortho_check .eqv. .true.) then

                    ! no need to print all this
                    call ortho_check_all("UMO", .false., orthotol)
                    print "(A32, E12.6, A10, 2I3)", "Largest deviation of overlap = ", &
                        & maxval(orthomat), "of pairs ", maxloc(orthomat)

                end if

            end if

            ! UMO diag
            if (UMOdiag .eqv. .TRUE. ) then

                print*, "UMO DIAG"
                print "(3A24)", "UMO(i, iocc)",  "refMO(i, iocc)", "diff(UMO-refCMO)"

                modiff = 0.0d0

                do i=1, wnnprim

                    modiff = modiff + abs(UMO(i,iocc) - refmo(i, iocc))

                    print "(3E24.16)", UMO(i, iocc), refmo(i, iocc), UMO(i, iocc) - refmo(i,iocc)

                end do

                print "(A24,E24.16)", "sum(modiff) = " , modiff

            end if

            print*, "=============================================="
            print*, ""

            ! update converged_alphas
            converged_alphas(iocc,ivirt) = converged_alphas(iocc,ivirt) + alpha_sweep

        end do !ivirt

        print*, "*****"
        print "(A30, I4)", "Optimal alphas for iocc = ", iocc

        print "(A4, A15, 2A24)", "MO", "sym", "alpha", "MAD"
        do ivirt=1, wnnprim

            print "(I4, A15, 2E24.16)", ivirt, symlabels(ivirt), alphas(ivirt), best(ivirt)

        end do
        print*, "*****"

    end do !iocc

    call cpu_time(jacobisweep_time_finish)

    ! saving best rmsd
    best_sweep_err = best(wnnprim)

    print '(A30, f16.2)', "Walltime jacobi [s] = ", jacobisweep_time_finish - jacobisweep_time_start

    ! write to file fort.21 after each sweep
    open(unit=13, status = "replace", file="fort.21")

    write(13, "(4E24.16)") converged_alphas

    close(unit=13)
    

end subroutine jacobisweep


! subroutine to update UMOs given a pair 
! i and j of MO indices and an angle alpha
subroutine update_umos(ino, inv, inalpha)

    use orbital_variables

    use sweep_variables

    integer                     :: ino, inv

    real*8                      :: inalpha

    integer                     :: iprim                         

    real*8                      :: tmpio, tmpiv

    do iprim=1, wnnprim

        tmpio = UMO(iprim, ino)
        tmpiv = UMO(iprim, inv)
        
        UMO(iprim,ino) = cos(inalpha)*tmpio + sin(inalpha)*tmpiv

        UMO(iprim,inv) = -sin(inalpha)*tmpio + cos(inalpha)*tmpiv

    end do

end subroutine update_umos
