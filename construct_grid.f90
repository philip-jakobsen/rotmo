!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to create integration grid and write to file
! 
subroutine collectgrid(inprintflag)

    use common_variables

    use grid_variables

    implicit none

    integer                     :: i
    logical                     :: inprintflag

    ! read angular grid from file "angular_grid/<order>"
    ! the weights are scaled accordingly
    call agrid(nagrid)

    ! construct grid and accompanying integration and fuzzy weights
    call makegrid(nrgrid, nagrid, lebedev_x, lebedev_y, lebedev_z, lebedev_w, natoms, atnums, coords)

    ! print cartesian grid to file fort.7
    if (inprintflag .eqv. .true.) then
        print*, "Writing ", npoints, "points to cartesian grid points to fort.7"
        do i=1, npoints
            write (7, '(3E24.16)') grid(1:3,i)
        end do
        print*, "================================================================"
    end if

    print*, ""
    print*, ""
    print*, ""

end subroutine collectgrid

