!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! module for common variables related to grid contruction
!
module grid_variables

    implicit none

    public

    ! number of radial and angular points
    integer                             :: nrgrid, nagrid
    
    ! total number of points
    integer                             :: npoints

    ! first used in load_lebedev
    ! lebedev angular grid points (x,y,z) and associated weights w
    real*8, dimension(:), allocatable   :: lebedev_x, lebedev_y, lebedev_z, lebedev_w

    ! first used in integ
    ! resulting grid (x,y,z) and weights w
    real*8, dimension(:,:), allocatable :: grid

    ! for loading data with load_dens.f90
    real*8, dimension(:), allocatable   :: data_array

    ! number of electrons
    real*8                              :: nelectrons

    save

end module

