!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
! IO units 5 = stdin (parseinp.f90)
! 6 = stdout

! 1x = DALTON
!
! 10 = dalton basis file (loadbasis.f90)

! 14 = densities from evaldens
! 15 = densities from evalrefdens
! 16 = densities from evalUdens2
! 17 = densities from evalaodens
! 18 = densities from evalmodens
! 19 = densities from evalrefaodens
! 
program main

    use common_variables

    use grid_variables

    use orbital_variables

    use sweep_variables

    use fit_variables

    ! for various omp stuff
    use omp_lib

    implicit none

    integer :: i, j, sweepiter, doublefactfunc

    logical :: debug, test, printflag, evald, evalrefd, orthonorm_check

    real*8 :: pi

    character(8)  :: date
    character(10) :: time
    character(5)  :: zone
    integer,dimension(8) :: date_values

    real*8 :: convtol


    real*8 :: main_time_start, main_time_finish

    !real*8, dimension(:), allocatable :: alphas

    call date_and_time(date,time,zone,date_values)

    ! misc values
    pi = 4*atan(1.0d0)

    print*, ""
    print*, "================================================================"
    print*, ""
    print*, "ROTMO STARTING"
    print*, ""
    !print "(A1,I4,A1,I2,A1,I2,A1,I2,A1,I2)", " ", date_values(1), "-", date_values(2), "-", &
        !& date_values(3), " ", date_values(5), ":", date_values(6)
    print "(x,a,2x,a)", date, time
    print*, ""
    print "(A20,I3,A20)", "Running on ", omp_get_num_procs(), "openMP threads"
    print*, ""
    print*, "================================================================"
    print*, ""

    printflag = .false.

    print*, "Starting cpu timing"
    call cpu_time(main_time_start)
    print*, ""
    print*, ""

    writeflag = .true.


    ! read input file from stdin
    call parse

    if (frj_interface .eqv. .true.) writeflag = .false.

    ! constructing grid writes x y z to fort.7
    call collectgrid(writeflag)

    !basisdir = "/home/philip/speciale/dalton_basis/"

    debug = .true.

    test = .true.


    evald = .true.

    orthonorm_check = .true.
    
    ! read basis from dalton basis set file
    ! also assigning cartesian coordinates to each AO
    ! arguments: <filedirectory>, <filename>, <print>
    ! output:       "exponent list" allprim 
    !               "geometry matrix" allcoords, 
    !               "angular momentum matrix" allatoml2
    call readallbasis(basisdir, basisfilename, inp_print)

    ! read reference basis from dalton basis set file
    ! also assigning cartesian coordinates to each AO
    !
    ! arguments:    <filedirectory>, <filename>, <print>
    ! output:       "exponent list" refallprim 
    !               "geometry matrix" refallcoords, 
    !               "angular momentum matrix" refallatoml2
    call readallrefbasis(basisdir, refbasisfilename, inp_print)

    ! making double factorials
    do i=1,30

        ! offset of 2, lowest value is -1
        doublefact(i) = doublefactfunc(i-2)

    end do

    ! read overlap matrix from gamess
    ! arguments: <filename>, <dimension>, <print>
    ! output:       "overlap matrix" overlapmatrix
    call readgamessoverlap(overlapfilename, wnnprim, inp_print)

    ! load AO -> MO coefficients from file
    ! last argument is print MOs
    ! arguments: <filename>, <dimension> <print>
    ! output:       "C matrix" MO
    call loadgamessmolorbs(mopun, wnnprim, inp_print)

    ! load reference AO -> MO coefficients 
    ! arguments: <filename>, <dimension> <print>
    ! output:       "reference C matrix" refMO
    call loadgamessrefmolorbs(refmopun, refnnprim, inp_print)

    ! load 1-electron density matrix in AO basis 
    ! arguments: <filename>, <dimension> <print>
    ! output:       "density matrix in AO basis" gamessAOdensitymatrix
    call loadgamessAOD(densitymatrixfilename, wnnprim, inp_print)

    ! load reference 1-electron density matrix in AO basis 
    ! arguments: <filename>, <dimension> <print>
    ! output:       "reference density matrix in AO basis" gamessCIAOdensitymatrix
    call loadgamessCIAOD(refdensitymatrixfilename, refnnprim, inp_print)

    ! load symmetry labels, assuming file "SYM" in same directory as HF CMO etc
    ! arguments: <filename>, <print>
    ! output:       "list of symmetry labels for each MO" symlabels
    call loadsym(symfilename, inp_print)

    ! manually make 1-electron density matrix in MO basis
    ! obviously only works for restricted HF systems
    print*, "Manually making density matrix in MO basis (only RHF)"
    allocate(densitymatrix(wnnprim,wnnprim))
    densitymatrix = 0.0d0
    do i=1, nocc 
        densitymatrix(i,i) = 2.0d0
    end do

    ! temporarily disabling this when testing NOs
    print*, "Checking if D matrices in HF are correct..."
    print*, "Condition: (C.D(MO)).C^T = D(AO) "

    if (sum(matmul(matmul(MO,densitymatrix),transpose(MO))-gamessAOdensitymatrix) < 1.0d-12) then

        print*, "Deviation = ", sum(matmul(matmul(MO,densitymatrix),transpose(MO))-gamessAOdensitymatrix)
        print*, "Density matrices for HF looks good to me"

    else

        print*, "Deviations = ", sum(matmul(matmul(MO,densitymatrix),transpose(MO))-gamessAOdensitymatrix)

        ! disable this is using already rotated orbitals
        !error stop "Something is wrong, D(AO) -> D(MO) transformation does not result in the generated D(MO)"

    end if

    print*, "Evaluating HF densities on grid in AO basis"
    ! evaluate densities on grid in AO basis
    ! arguments: <threshold for element D(i,j)>, <write>
    ! output:      "density on grid" aodens, if write also x,y,z,rho,w to file fort.17
    call evalaodens(dtol, writeflag)

    ! check number of electrons
    ! arguments: <density on grid>, <printflag>
    call sumdens(aodens, .true.)
    print*, ""
    print*, ""
    print*, ""

    print*, "Evaluating HF densities on grid in MO basis"
    ! evaluate densities on grid in MO basis
    ! arguments: <thresh for D(i,j) element> <write> 
    ! outout:      "density on grid" dens, if write also x,y,z,rho,w to file fort.14
    call evaldens(dtol, writeflag)

    ! check number of electrons
    call sumdens(dens, .true.)
    print*, ""
    print*, ""
    print*, ""
    print "(A30, E12.6)", "Accuracy of integration = ", abs(dble(sum(atnums)) - nelectrons)

    ! check orthonormality of MOs
    if (orthonorm_check .eqv. .true.) then

        print*, "================================================================"
        print*, "Checking orthonormality of MOs"

        ! arguments: <type>, <print>, <tolerance>
        ! <type> is either "MO " or "UMO"
        call ortho_check_all("MO ", .false., orthotol)

        print "(A32, E12.6, A10, 2I3)", "Largest deviation of overlap = ", maxval(orthomat), "of pairs ", maxloc(orthomat)

    end if

    evalrefd = .true.

    ! generate reference densities on grid
    ! and write to fort.15
    if (evalrefd .eqv. .true.) then

        call restart_refdens()

    end if
        
    allocate(converged_alphas(nocc,wnnprim))
    allocate(converged_alphas_previous(nocc,wnnprim))
    converged_alphas = 0.0d0


    ! load restarted alphas from fort.90
    if (RESTART .eqv. .TRUE.) then

        call restart_alphas()

    end if

    allocate(hist_alphas(nocc,wnnprim,maxsweepiter))
    hist_alphas = 0.0d0

    if (frj_interface .eqv. .true.) then

        if (allocated(UMO) .eqv. .false.) then
            
            allocate(UMO(wnnprim,wnnprim))

            UMO = MO

        end if

        call opt_interface(.true.,1)

        print*, "Interface calculation complete"
        
        call date_and_time(date,time,zone,date_values)
        
        print "(2A20)", date, time

        return

    end if


    !allocate(UMO(wnnprim,wnnprim))
    !UMO = MO

    !allocate(alphas(wnnprim))
    !alphas = 0.0d0

    !do i=1, 400

        !do j=1, 200

            !alphas(3) = -dble(i)/10000d0
            !alphas(11) = dble(j)/100000d0

            !call evalUdens3(dtol, .false., alphas, 1, 3)

            !call calcerrf(udens, refaodens, .false.)

            !!call gradhess(-dble(i)/100000d0,1,3)
            !!call grad(-dble(i)/100000d0,1,3)

            !write(99, "(E24.16, A4, E24.16, A4, E24.16)") alphas(3), ",", alphas(11), ",", errf_value

            !deallocate(udens)

        !end do

    !end do

    !return



    if (opttype .ne. "NONE  ") then 
        

        do sweepiter=1, maxsweepiter

            ! convergence check
            if (sweepiter .ge. 2 .and. all(converged_alphas .eq. converged_alphas_previous)) then
                
                print*, "No change since last sweep iteration"

                ! need this for restarting
                if (RESTART .eqv. .TRUE. ) then

                    converged_alphas = converged_alphas_previous

                end if

                exit

            else

                converged_alphas_previous = converged_alphas

            end if
            
            print*, "Sweep number = ", sweepiter

            previous_best_sweep_err = best_sweep_err

            if (opttype .eq. "JACOBI") then

                ! perform jacobi sweep procedure by rotating MOs
                ! arguments: <iteration number>, <writeflag>, <debugflag>
                ! output: 
                call jacobisweep(sweepiter, .false., .false.)

                convtol = sweeptol

            else if (opttype .eq. "NEWTON") then

                ! perform NR sweep procedure by rotating MOs
                ! arguments: <iteration number>, <writeflag>, <debugflag>
                ! output: 
                call newton(sweepiter, .false., .true.)

                convtol = sweeptol

            !else if (opttype .eq. "BFGS  ") then

                !! perform NR sweep procedure by rotating MOs
                !! arguments: <iteration number>, <writeflag>, <debugflag>
                !! output: 
                !call BFGS_opt(sweepiter, .false., .true.)

                !convtol = BFGStol

            end if


            ! check convergence
            if (previous_best_sweep_err - best_sweep_err .lt. convtol) then

                print*, ""
                print*, "*****"
                print*, "Sweep procedure converged"
                print "(A20, E10.4)", "Last change = ", abs(best_sweep_err - previous_best_sweep_err)
                print*, "*****"
                print*, ""

                exit

            else

                if (sweepiter .ge. 2) print "(A45, E16.8)", "Reduction in error from last sweep = ", &
                    & previous_best_sweep_err - best_sweep_err

            end if

            ! conv plot
            write(40, "(I5, E24.16)") sweepiter, best_sweep_err

            if (extrapolateflag .eqv. .true. ) then
                
                if ( sweepiter .eq. ex_end) then

                    ! exponential extrapolation
                    call fit()

                 end if

            end if

            if (sweepiter .eq. maxsweepiter) then

                print*, "Max sweep iter of", maxsweepiter, "reached!"
                error stop "NOT converged"

            end if

        end do

    end if



    print*, "==============================================================="
    print*, "RESULTS"
    print*, ""
    print*, "Sweep converged after", sweepiter, "sweep iterations"
    print*, ""
    print "(A20, E24.16)", "Optimial err", best_sweep_err

    print*, "Converged alphas"

    ! add alphas from restart
    if (RESTART .eqv. .true.) then

        converged_alphas = converged_alphas + converged_alphas_restart

    end if

    do i=1, nocc

        print*, "iocc = 1"    

        print*, "ivirt, sym(ivirt), alpha"
        do j=1, wnnprim
        
            print "(I4, A10, E24.16)", j, symlabels(j), converged_alphas(i, j)

        end do


        print*, ""
        print*, ""

    end do


    print*, "Writing converged alphas to fort.21"
    write(21, "(4E24.16)") converged_alphas

    print*, "Writing hist_alphas to fort.22"
    write(22, "(3E24.16)") hist_alphas

    print*, "Writing rotated UMO to fort.99"
    write(99, "(4E24.16)") UMO

    print*, ""
    call cpu_time(main_time_finish)
    print '(A20, f12.2)', "Walltime [s] = ", main_time_finish - main_time_start



    call date_and_time(date,time,zone,date_values)


    print*, ""
    print*, "================================================================"
    print*, ""
    print*, "ROTMO ENDED GRACEFULLY"
    print*, ""
    print "(2A20)", date, time
    print*, ""
    print*, "================================================================"
    print*, ""


end program main

