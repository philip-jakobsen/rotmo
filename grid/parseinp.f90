!=========================================================
!
! subroutine to parse input file from stdin
! 
!==========================================================

subroutine parse

    use common_variables

    use grid_variables

    implicit none

    integer :: nlines
    integer                                            :: i, iatoms
    character(len=512), dimension(50)                  :: inpline

    ! read input from stdin
    ! first line is number of lines in input file
    read(5, *) nlines

    rewind(5)

    do i=1,nlines
        read(5, '(A)') inpline(i)
        !print*,line
    end do

    ! parse input parameters
    read(inpline(3), *) nrgrid
    read(inpline(5), '(I4)') nagrid
    read(inpline(7), *) natoms

    allocate(atnums(natoms))
    allocate(coords(3,natoms))
    allocate(basisfilename(natoms))
    allocate(refbasisfilename(natoms))
    allocate(atlabel(natoms))

    ! read atom number and xyz coordinates and basis and reference basis
    do iatoms=1, natoms
        read(inpline(8+iatoms), *) atlabel(iatoms), atnums(iatoms), coords(1,iatoms), coords(2, iatoms), coords(3, iatoms)
    end do



    npoints = nrgrid*nagrid*natoms

    ! print input parameters
    print*, "================================================================"
    print*, "input parameters from input file"
    print*, "================================================================"
    print '(A20, I8)', "natoms = ", natoms
    print '(A20, I8)', "nrgrid = ", nrgrid
    print '(A20, I8)', "nagrid = ", nagrid
    print '(A20, I8)', "npoints = ", npoints
    print '(A60)', adjustl("label atnum geometry (x, y, z)               basis    refbasis")
    
    do iatoms=1, natoms
        print '(A5, I3, 3f12.6)', atlabel(iatoms), atnums(iatoms), coords(1,iatoms), coords(2,iatoms), coords(3,iatoms)
                                         
    end do

    print*, "================================================================"
    print*, ""
    print*, ""

end subroutine parse

