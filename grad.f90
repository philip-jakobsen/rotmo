!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutines to calculate gradient and 1-dimensional hessian and NR step
!
subroutine NRstep(inalpha, ino, inv)

    use sweep_variables

    implicit none

    real*8 :: inalpha
    integer :: ino, inv

    call gradhess(inalpha, ino, inv)

    NR_step = - dFdT / dF2dT2

end subroutine


! calculate gradient
subroutine calc_grad(inalpha, ino, inv)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    implicit none

    real*8, intent(in)              :: inalpha

    real*8, dimension(npoints) :: grad_points

    real*8, dimension(:,:), allocatable :: UMO_copy

    integer, intent(in)                     :: ino, inv

    real*8                      :: tmp, tmpm
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8 :: tmp2

    real*8 :: tmpi

    real*8 :: c, s

    real*8 :: nel, phi1

    c = cos(inalpha)
    s = sin(inalpha)

    nel = dble(sum(atnums))

    dFdT = 0.0d0

    grad_points = 0.0d0

    allocate(UMO_copy(wnnprim, wnnprim))

    UMO_copy = UMO


    !$omp parallel do private(tmp, tmp2, tmpm, tmpi, chim, chii, MOmi, mao, MOii, iao,ipoint,phi1)

    do ipoint=1, npoints

        tmp = 0.0d0

        !tmp2 = 0.0d0

        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)

            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            MOii = UMO_copy(iao, inv)

            tmpi = tmpi + (chii*MOii)

        end do

        ! frj eq 11, missing term not included: 4*s**4*tmpi**4-4*s**4*tmpi**2
        !tmp = 16.0d0*(-c**3*s*tmpm**4+s**3*c*tmpi**4 + 3.0d0*(c**3*s-c*s**3)*tmpm**2*tmpi**2 + &
            !(c**4-3.0d0*c**2*s**2)*tmpi**3*tmpm + (3.0d0*c**2*s**2-s**4)*tmpm*tmpi**3) - 8*refaodens(ipoint) * &
            !& (-c*s*tmpm**2+c*s*tmpi**2 + (c**2-s**2)*tmpm*tmpi)

        ! mathematica
        !tmp2 = tmpi*c - tmpm*s)*(tmpm*c+tmpi*s)*(refaodens(ipoint)-2.0d0*(tmpm*c+tmpi*s)**2)

        phi1 = (c*tmpm+s*tmpi)

        ! frj2, eq 7 gradients2.docx
        grad_points(ipoint) = (refaodens(ipoint) - 2*phi1**2)*phi1*(-s*tmpm+c*tmpi)

    end do

    !$omp end parallel do

    deallocate(UMO_copy)

    ! integrate over points
    dFdT = -8.0d0 / nel * sum(grad_points)

end subroutine calc_grad


! calculate 1-dimensional hessian
subroutine hess(inalpha, ino, inv)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    implicit none

    real*8, intent(in)              :: inalpha

    real*8, dimension(npoints) :: hess_points

    real*8, dimension(:,:), allocatable :: UMO_copy

    integer, intent(in)                     :: ino, inv

    real*8                      :: tmp, tmpm
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8 :: tmp2

    real*8 :: tmpi

    real*8 :: c, s

    real*8 :: nel

    dF2dT2 = 0.0d0

    allocate(UMO_copy(wnnprim, wnnprim))

    UMO_copy = UMO

    c = cos(inalpha)
    s = sin(inalpha)

    nel = dble(sum(atnums))

    !$omp parallel do private(tmp, tmp2, tmpm, tmpi, chim, chii, MOmi, mao, MOii, iao)

    do ipoint=1, npoints

        tmp = 0.0d0

        tmp2 = 0.0d0

        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)

            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            MOii = UMO_copy(iao, inv)

            tmpi = tmpi + (chii*MOii)

        end do

        ! eq 12
        !tmp = 16.0d0*((3.0d0*c**2*s**2 -c**4)*tmpm**4 + (3.0d0*c**2*s**2-s**4)*tmpi**4 + &
            !& 3.0d0*(c**4-s**4)*tmpm**2*tmpi**2 + 2.0d0*(3.0d0*c*s**2-5.0d0*c**3*s)*tmpm**3*tmpi + &
            !& 2.0d0*(3.0d0*c**3*s-5.0d0*c*s**3)*tmpm*tmpi**3) - 8.0d0*refaodens(ipoint)*((s**2-c**2)*tmpm**2 &
            !& + (c**2-s**2)*tmpi**2 - 4.0d0*c*s*tmpm*tmpi)
            

        ! mathematica
        !tmp2 = 32.0d0*(tmpi*c-tmpm*s)**2*(tmpm*c+tmpi*s)**2 - &
            !& 8.0d0*(tmpi*c-tmpm*s)**2*(refaodens(ipoint) - 2.0d0*(tmpm*c+tmpi*s)**2) - &
            !& 8.0d0*(-tmpm*c-tmpi*s)*(tmpm*c+tmpi*s)*(refaodens(ipoint)-2.0d0*(tmpm*c+tmpi*s)**2)
            

        ! frj2
        !tmp2 = (refaodens(ipoint)-2*tmpm**2)*(tmpm**2-tmpi**2)+4*tmpm**2*tmpi**2
        !tmp2 = (refaodens-2*(c*tmpm+s*tmpi))*(c*tmpm+s*tmpi)**2+(4*())

        hess_points(ipoint) = hess_points(ipoint) + tmp2

    end do

    !$omp end parallel do

    deallocate(UMO_copy)

    ! integrate over points
    dF2dT2 = 8.0d0/nel * sum(hess_points)


end subroutine hess




subroutine gradhess(inalpha, ino, inv)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    implicit none

    real*8, intent(in)              :: inalpha

    real*8, dimension(npoints) :: hess_points
    real*8, dimension(npoints) :: grad_points

    real*8, dimension(:,:), allocatable :: UMO_copy

    integer, intent(in)                     :: ino, inv

    real*8                      :: tmp, tmpm
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8 :: tmp2

    real*8 :: tmpi

    real*8 :: c, s

    real*8 :: nel

    real*8 :: phi1

    !print*, inalpha

    dFdT = 0.0d0
    dF2dT2 = 0.0d0

    allocate(UMO_copy(wnnprim, wnnprim))

    UMO_copy = UMO

    c = cos(inalpha)
    s = sin(inalpha)

    nel = dble(sum(atnums))

    !$omp parallel do private(tmp, tmp2, tmpm, tmpi, chim, chii, MOmi, mao, MOii, iao)

    do ipoint=1, npoints

        tmp = 0.0d0

        tmp2 = 0.0d0

        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)

            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            MOii = UMO_copy(iao, inv)

            tmpi = tmpi + (chii*MOii)

        end do

        phi1 = (c*tmpm+s*tmpi)

        ! frj2
        grad_points(ipoint) = (refaodens(ipoint) - 2*phi1**2)*phi1*(-s*tmpm+c*tmpi)
        hess_points(ipoint) = (refaodens(ipoint) - 2*phi1**2)*phi1**2+(4*phi1**2-(refaodens(ipoint) - phi1**2)) &
            & * (-s*tmpm+c*tmpi)**2

    end do

    !$omp end parallel do

    deallocate(UMO_copy)

    ! integrate over points
    dFdT = -8.0d0/nel * sum(grad_points)
    dF2dT2 = 8.0d0/nel * sum(hess_points)


end subroutine gradhess

subroutine full_hess(ino, ini, inj, inalphai, inalphaj)


    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    real*8, intent(in)              :: inalphai, inalphaj

    real*8, dimension(npoints) :: hess_points

    real*8, dimension(:,:), allocatable :: UMO_copy

    integer, intent(in)                     :: ino, ini, inj

    real*8                      :: tmpm, tmpi, tmpj
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8                      :: chij
    real*8                      :: MOji

    real*8 :: ci, cj, si, sj

    real*8 :: nel


    !print*, inalpha

    dFdT = 0.0d0
    dF2dT2 = 0.0d0

    allocate(UMO_copy(wnnprim, wnnprim))

    UMO_copy = UMO

    ci = cos(inalphai)
    cj = cos(inalphaj)

    si = sin(inalphai)
    sj = sin(inalphaj)

    nel = dble(sum(atnums))

    !$omp parallel do private(tmpm, tmpi, tmpj, chim, chii, chij, MOmi, MOii, MOji, mao, iao, jao, &
    !$omp & phi1, dphidth1,dphidth2,dphi2dth2)

    do ipoint=1, npoints


        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)

            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            MOii = UMO_copy(iao, ini)

            tmpi = tmpi + (chii*MOii)

        end do

        ! virtual j
        tmpj = 0.0d0
        do jao=1, wnnprim

            chij = evalcartfunc2(allprim(jao), grid(1:3,ipoint)- allcoords(:,jao), allatoml2(:,jao)) 
            MOji = UMO_copy(jao, inj)

            tmpj = tmpj + (chij*MOji)

        end do

        ! following is implemented first term in "or" equations in
        ! gradients3.doc

        phi1 = (ci*cj*tmpm+si*tmpi+ci*sj*tmpj)

        ! eq 11 
        dphidth1 = (-si*cj*tmpm+ci*tmpi-si*sj*tmpj)

        ! eq 12
        dphidth2 = (-ci*sj*tmpm+ci*cj*tmpj)

        ! eq 26
        dphi2dth2 = si*sj*tmpm-si*cj*tmpj

        pref = refaodens(ipoint)


        ! frj2
        hess_points(ipoint) = (4.0d0*phi1**2-(refaodens(ipoint)-2.0d0*phi1**2))*(dphidth1*dphidth2) - &
            & (refaodens(ipoint)-2.0d0*phi1**2)*phi1*dphi2dth2

        ! frj, zero angle
        !hess_points(ipoint) = (4*tmpm**2-(refaodens(ipoint)-2*phi1**2))*tmpi*tmpj

        ! mathematica
        !hess_points(ipoint) = 32*(tmpj*ci*cj-tmpm*ci*sj)*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)**2* &
            !& (tmpi*ci-tmpm*cj*si-tmpj*si*sj)-8*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)*(-tmpj*cj*si+tmpm*si*sj) * &
            !& (pref-2*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)**2)- 8*(tmpj*ci*cj-tmpm*ci*sj)*(tmpi*ci-tmpm*cj*si- tmpj*si*sj)* &
            !& (pref-2*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)**2)


    end do

    !$omp end parallel do

    deallocate(UMO_copy)

    ! sum over points
    hij = 8.0d0/nel*sum(hess_points)
    
end subroutine full_hess

! calculate gradient
subroutine calc_grad3(inalphas, ino, inv)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    implicit none

    real*8, dimension(wnnprim), intent(in)              :: inalphas

    real*8, dimension(npoints) :: grad_points

    real*8, dimension(:,:), allocatable :: UMO_copy

    integer, intent(in)                     :: ino, inv

    real*8                      :: tmp, tmpm
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8 :: tmp2

    real*8 :: tmpi

    real*8 :: c, s

    real*8 :: nel, phi1

    integer :: ivirt, iprim
    real*8 :: tmpio, tmpiv

    c = cos(inalphas(inv))
    s = sin(inalphas(inv))

    nel = dble(sum(atnums))

    dFdT = 0.0d0

    grad_points = 0.0d0

    allocate(UMO_copy(wnnprim, wnnprim))

    UMO_copy = UMO


    ! rotate all the other orbitals
    do ivirt=1, wnnprim

        if (ivirt .ne. inv) then

            do iprim=1, wnnprim

                tmpio = UMO_copy(iprim, ino)
                tmpiv = UMO_copy(iprim, ivirt)
                
                UMO_copy(iprim,ino) = cos(inalphas(ivirt))*tmpio + sin(inalphas(ivirt))*tmpiv

                UMO_copy(iprim,ivirt) = -sin(inalphas(ivirt))*tmpio + cos(inalphas(ivirt))*tmpiv

            end do

        end if

    end do
        
    !$omp parallel do private(tmp, tmp2, tmpm, tmpi, chim, chii, MOmi, mao, MOii, iao,ipoint,phi1)

    do ipoint=1, npoints

        tmp = 0.0d0

        !tmp2 = 0.0d0

        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)

            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            MOii = UMO_copy(iao, inv)

            tmpi = tmpi + (chii*MOii)

        end do

        ! frj eq 11, missing term not included: 4*s**4*tmpi**4-4*s**4*tmpi**2
        !tmp = 16.0d0*(-c**3*s*tmpm**4+s**3*c*tmpi**4 + 3.0d0*(c**3*s-c*s**3)*tmpm**2*tmpi**2 + &
            !(c**4-3.0d0*c**2*s**2)*tmpi**3*tmpm + (3.0d0*c**2*s**2-s**4)*tmpm*tmpi**3) - 8*refaodens(ipoint) * &
            !& (-c*s*tmpm**2+c*s*tmpi**2 + (c**2-s**2)*tmpm*tmpi)

        ! mathematica
        !tmp2 = tmpi*c - tmpm*s)*(tmpm*c+tmpi*s)*(refaodens(ipoint)-2.0d0*(tmpm*c+tmpi*s)**2)

        phi1 = (c*tmpm+s*tmpi)

        ! frj2, eq 7 gradients2.docx
        grad_points(ipoint) = (refaodens(ipoint) - 2*phi1**2)*phi1*(-s*tmpm+c*tmpi)

    end do

    !$omp end parallel do

    deallocate(UMO_copy)

    ! integrate over points
    dFdT = -8.0d0 / nel * sum(grad_points)

end subroutine calc_grad3

subroutine full_hess3(ino, ini, inj, inalphas)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    real*8, dimension(wnnprim), intent(in)              :: inalphas

    real*8, dimension(npoints) :: hess_points

    real*8, dimension(:,:), allocatable :: UMO_copy

    integer, intent(in)                     :: ino, ini, inj

    real*8                      :: tmpm, tmpi, tmpj
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8                      :: chij
    real*8                      :: MOji

    real*8 :: ci, cj, si, sj

    real*8 :: nel

    integer :: ivirt, iprim
    real*8 :: tmpio, tmpiv

    !print*, inalpha

    dF2dT2 = 0.0d0

    allocate(UMO_copy(wnnprim, wnnprim))

    UMO_copy = UMO

    ! rotate all the other orbitals
    do ivirt=1, wnnprim

        !if (ivirt .ne. ini .or. ivirt .ne. inj) then
        
        if (ivirt .eq. ini) cycle

        if (ivirt .eq. inj) cycle

            do iprim=1, wnnprim

                tmpio = UMO_copy(iprim, ino)
                tmpiv = UMO_copy(iprim, ivirt)
                
                UMO_copy(iprim,ino) = cos(inalphas(ivirt))*tmpio + sin(inalphas(ivirt))*tmpiv

                UMO_copy(iprim,ivirt) = -sin(inalphas(ivirt))*tmpio + cos(inalphas(ivirt))*tmpiv

            end do

        !end if

    end do

    ci = cos(inalphas(ini))
    cj = cos(inalphas(inj))

    si = sin(inalphas(ini))
    sj = sin(inalphas(inj))

    nel = dble(sum(atnums))

    !$omp parallel do private(tmpm, tmpi, tmpj, chim, chii, chij, MOmi, MOii, MOji, mao, iao, jao, &
    !$omp & phi1, dphidth1,dphidth2,dphi2dth2)

    do ipoint=1, npoints


        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)

            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            MOii = UMO_copy(iao, ini)

            tmpi = tmpi + (chii*MOii)

        end do

        ! virtual j
        tmpj = 0.0d0
        do jao=1, wnnprim

            chij = evalcartfunc2(allprim(jao), grid(1:3,ipoint)- allcoords(:,jao), allatoml2(:,jao)) 
            MOji = UMO_copy(jao, inj)

            tmpj = tmpj + (chij*MOji)

        end do

        ! following is implemented first term in "or" equations in
        ! gradients3.doc

        phi1 = (ci*cj*tmpm+si*tmpi+ci*sj*tmpj)

        ! eq 11 
        dphidth1 = (-si*cj*tmpm+ci*tmpi-si*sj*tmpj)

        ! eq 12
        dphidth2 = (-ci*sj*tmpm+ci*cj*tmpj)

        ! eq 26
        dphi2dth2 = si*sj*tmpm-si*cj*tmpj

        pref = refaodens(ipoint)


        ! frj2
        hess_points(ipoint) = (4.0d0*phi1**2-(refaodens(ipoint)-2.0d0*phi1**2))*(dphidth1*dphidth2) - &
            & (refaodens(ipoint)-2.0d0*phi1**2)*phi1*dphi2dth2

        ! frj, zero angle
        !hess_points(ipoint) = (4*tmpm**2-(refaodens(ipoint)-2*phi1**2))*tmpi*tmpj

        ! mathematica
        !hess_points(ipoint) = 32*(tmpj*ci*cj-tmpm*ci*sj)*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)**2* &
            !& (tmpi*ci-tmpm*cj*si-tmpj*si*sj)-8*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)*(-tmpj*cj*si+tmpm*si*sj) * &
            !& (pref-2*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)**2)- 8*(tmpj*ci*cj-tmpm*ci*sj)*(tmpi*ci-tmpm*cj*si- tmpj*si*sj)* &
            !& (pref-2*(tmpm*ci*cj+tmpi*si+tmpj*ci*sj)**2)


    end do

    !$omp end parallel do

    deallocate(UMO_copy)

    ! sum over points
    hij = 8.0d0/nel*sum(hess_points)
    
end subroutine full_hess3

! calculate gradient
subroutine calc_grad4(inalphas, ino, inv)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    implicit none

    real*8, dimension(wnnprim), intent(in)              :: inalphas

    real*8, dimension(npoints) :: grad_points

    real*8, dimension(:,:), allocatable :: UMO_copy
    real*8, dimension(:,:), allocatable :: Umat, Umatgrad
    real*8, dimension(:,:), allocatable :: Vmat, Wmat

    integer, intent(in)                     :: ino, inv

    real*8                      :: tmpi, tmpm
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8 :: nel, phi1

    integer :: iprim, i,j

    nel = dble(sum(atnums))

    dFdT = 0.0d0

    grad_points = 0.0d0

    allocate(UMO_copy(wnnprim, wnnprim))
    UMO_copy = 0.0d0

    allocate(Umat(wnnprim, wnnprim))
    call form_umat(inalphas, Umat)

    allocate(Umatgrad(wnnprim, wnnprim))
    Umatgrad = 0.0d0
    Umatgrad(ino,inv) =  1.0d0
    Umatgrad(inv,ino) = -1.0d0
    Vmat = matmul(Umat,Umatgrad)

    allocate(Wmat(wnnprim, wnnprim))
    Wmat = 0.0d0

    ! rotate all the orbitals
    !UMO_copy = matmul(transpose(Umat),UMO)
    do i=1, wnnprim
      do j=1, wnnprim
            do iprim=1, wnnprim
                UMO_copy(iprim,i) = UMO_copy(iprim,i) + umat(i,j)*UMO(iprim, j)
                Wmat(iprim,i) = Wmat(iprim,i) + Vmat(i,j)*UMO(iprim, j)
            end do
      end do
    end do
        
    !$omp parallel do private(tmpm, tmpi, chim, chii, MOmi, mao, MOii, iao,ipoint,phi1)

    do ipoint=1, npoints

        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)
            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        ! note that when using the rotated gradient term, it is the occupied index
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            !MOii = UMO_copy(iao, inv)
            !MOii = Wmat(iao, inv)
            MOii = Wmat(iao, ino)
            tmpi = tmpi + (chii*MOii)

        end do

        phi1 = tmpm

        grad_points(ipoint) = (refaodens(ipoint) - 2*phi1**2)*phi1*tmpi

    end do

    !$omp end parallel do

    deallocate(UMO_copy)
    deallocate(Umat)
    deallocate(Vmat)
    deallocate(Wmat)

    ! integrate over points
    dFdT = -8.0d0 / nel * sum(grad_points)

end subroutine calc_grad4

subroutine full_hess4(ino, ini, inj, inalphas)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    real*8, dimension(wnnprim), intent(in)              :: inalphas

    real*8, dimension(npoints) :: hess_points

    real*8, dimension(:,:), allocatable :: UMO_copy
    real*8, dimension(:,:), allocatable :: Umat
    real*8, dimension(:,:), allocatable :: Uimatgrad, Vimat, Wimat
    real*8, dimension(:,:), allocatable :: Ujmatgrad, Vjmat, Wjmat

    integer, intent(in)                     :: ino, ini, inj

    real*8                      :: tmpm, tmpi, tmpj
    integer                     :: mao, iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8                      :: chii
    real*8                      :: MOii

    real*8                      :: chij
    real*8                      :: MOji

    real*8 :: delrho

    real*8 :: nel

    integer :: iprim

    dF2dT2 = 0.0d0
    allocate(UMO_copy(wnnprim, wnnprim))
    UMO_copy = 0.0d0

    allocate(Umat(wnnprim, wnnprim))
    call form_umat(inalphas, Umat)

    allocate(Uimatgrad(wnnprim, wnnprim))
    Uimatgrad = 0.0d0
    Uimatgrad(ino,ini) =  1.0d0
    Uimatgrad(ini,ino) = -1.0d0
    Vimat = matmul(Umat,Uimatgrad)

    allocate(Ujmatgrad(wnnprim, wnnprim))
    Ujmatgrad = 0.0d0
    Ujmatgrad(ino,inj) =  1.0d0
    Ujmatgrad(inj,ino) = -1.0d0
    Vjmat = matmul(Umat,Ujmatgrad)

    allocate(Wimat(wnnprim, wnnprim))
    allocate(Wjmat(wnnprim, wnnprim))
    Wimat = 0.0d0
    Wjmat = 0.0d0

    ! rotate all the orbitals
    do i=1, wnnprim
      do j=1, wnnprim
            do iprim=1, wnnprim
                UMO_copy(iprim,i) = UMO_copy(iprim,i) + umat(i,j)*UMO(iprim, j)
                Wimat(iprim,i) = Wimat(iprim,i) + Vimat(i,j)*UMO(iprim, j)
                Wjmat(iprim,i) = Wjmat(iprim,i) + Vjmat(i,j)*UMO(iprim, j)
            end do
      end do
    end do

    nel = dble(sum(atnums))

    !$omp parallel do private(tmpm, tmpi, tmpj, chim, chii, chij, MOmi, MOii, MOji, mao, iao, jao, &
    !$omp & phi1)

    do ipoint=1, npoints

        ! occupied m
        tmpm = 0.0d0
        do mao=1, wnnprim

            chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
            MOmi = UMO_copy(mao, ino)
            tmpm = tmpm + (chim*MOmi)

        end do

        ! virtual i
        tmpi = 0.0d0
        do iao=1, wnnprim

            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            !MOii = UMO_copy(iao, ini)
            MOii = Wimat(iao, ino)
            tmpi = tmpi + (chii*MOii)

        end do

        ! virtual j
        tmpj = 0.0d0
        do jao=1, wnnprim

            chij = evalcartfunc2(allprim(jao), grid(1:3,ipoint)- allcoords(:,jao), allatoml2(:,jao)) 
            !MOji = UMO_copy(jao, inj)
            MOji = Wjmat(jao, ino)
            tmpj = tmpj + (chij*MOji)

        end do

        phi1 = tmpm
        pref = refaodens(ipoint)
        delrho = pref - 2.0d0*phi1**2

        hess_points(ipoint) = (4.0d0*phi1**2-delrho)*tmpi*tmpj 
        if (ini == inj) then
          hess_points(ipoint) = hess_points(ipoint) + delrho*phi1**2
        endif

    end do

    !$omp end parallel do

    deallocate(UMO_copy)
    deallocate(Umat)
    deallocate(Vimat)
    deallocate(Vjmat)
    deallocate(Wimat)
    deallocate(Wjmat)

    ! sum over points
    hij = 8.0d0/nel*sum(hess_points)
    
end subroutine full_hess4

! calculate unitary transformation matrix Umat from a skew-symmetric X-matrix
! containing the rotation angles
subroutine form_umat(inalphas, Umat)

    use orbital_variables

    implicit none

    real*8, dimension(wnnprim), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat

    real*8, dimension(:,:), allocatable :: Xmat,Vmat,Wmat

    real*8                      :: eps,tfac,vmax
    integer                     :: kmax,kiter,i

    !print*, "FRJ, entry   form_Umat"
    !print "(14E12.4)",inalphas
    ! allocate and initialize working matrices
    allocate(Xmat(wnnprim, wnnprim))
    allocate(Vmat(wnnprim, wnnprim))
    allocate(Wmat(wnnprim, wnnprim))

    Umat = 0.0d0
    Xmat = 0.0d0
    Vmat = 0.0d0
    Wmat = 0.0d0

    forall(i = 1:wnnprim) Umat(i,i) = 1.0d0
    ! assignment can be run from 2 or from ino+1
    forall(i = 1:wnnprim) Xmat(1,i) =  inalphas(i)
    forall(i = 1:wnnprim) Xmat(i,1) = -inalphas(i)
    Xmat(1,1) = 0.0d0

    ! initialize, always form U as I + X, then add terms until converged
    Vmat = Xmat
    Umat = Umat + Xmat

    ! form Umat by Taylor series, limit to kmax terms, terminate when converged
    ! to eps. Vmat = Xmat^n
    kmax = 30
    eps  = 1.0d-14

    do kiter=2, kmax
      tfac = 1.0d0/dble(kiter)
      Wmat = matmul(Vmat,Xmat)
      Vmat = Wmat*tfac
      vmax = maxval(abs(Vmat))
      !print*, "FRJ, form_Umat, iterating, vmax",kiter,vmax
      if (vmax > eps) then
        Umat = Umat + Vmat
        if (kiter == kmax)print*, "form_Umat hitting kmax",vmax,eps
      else
        exit
      endif
    end do

    deallocate(Xmat)
    deallocate(Vmat)
    deallocate(Wmat)

end subroutine form_umat

