!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
! subroutine to interface with Franks general optimization program
! input: fort.10
! output: hessian (fort.9), function value and gradient (fort.12)
subroutine opt_interface(debugflag, inocc)

    use common_variables

    use orbital_variables

    use sweep_variables

    implicit none

    logical                     :: debugflag

    real*8, dimension(:), allocatable  :: alphas, grad
    real*8, dimension(:,:), allocatable :: hess

    !real*8, dimension(wnnprim) :: alphavec

    integer                     :: inocc

    integer :: i, j

    real*8 :: radius
    integer :: mstep

    !real*8 :: tmp1, tmp2, tmp3, tmp4


    ! read dimension
    read(10, *) wnnprim

    ! allocation
    allocate(grad(wnnprim))
    allocate(alphas(wnnprim))
    allocate(hess(wnnprim,wnnprim))
    allocate(poptlst(wnnprim))

    ! read input parameters
    ! alphas, radius, mstep
    read(10, "(4E24.16)") alphas

    if (debugflag .eqv. .true.) then

        print*, "input alphas"

        print "(4E24.16)", alphas

    end if

    ! read radius and mstep
    read(10, *) radius, mstep

    if (debugflag .eqv. .true.) then

        print*, "radius mstep"

        print*, radius, mstep

    end if

    
    !! read poptlst
    !open(unit=20, file="popt.lst.rotmo", status="old")

    !do i=1, wnnprim

        !read(20, *) poptlst(i)

    !end do

    !close(unit=20)


    ! write back
    !write(13, )

    ! convert alphas into vector for evalUdens3

    !alphavec = 0.0d0
    !do i=1, wnnprim

        !alphavec(poptlst(i)) = alphas(i)

    !end do

    ! get function value
    !call evalUdens3(dtol, .false., alphas, inocc)
    !call calcerrf(udens, refaodens, .false.)
    !print*, " eval3 ErrF = ", errf_value

        !FRJ change
    call evalUdens4(dtol, .false., alphas, inocc)
    call calcerrf(udens, refaodens, .false.)
    !print*, " eval4 ErrF = ", errf_value

    deallocate(udens)

    if (debugflag .eqv. .true.) print*, "ErrF = ", errf_value

    ! get gradient
    grad = 0.0d0
    call calc_grad_vec(inocc, alphas, grad)

    ! write gradient vector
    write(12, "(E24.16)") errf_value
    write(12, "(E24.16)") grad

    if (debugflag .eqv. .true.) then
        
        print*, "grad"
        
        print "(E24.16)", grad

    end if

    call calc_hess_mat(inocc, alphas, hess, debugflag)

    ! write hessian matrix
    !write(32, *) iocc
    ! format and unit is from README
    !write(9, "(5E20.10)") hess

    ! testing with identity matrix as hessian
    !hess = 0.0d0
    !do j=1, wnnprim
        !hess(j,j) = 1.0d0
    !end do

    !write(9, "(4E24.16)") hess

    ! per README
    do i=1, wnnprim

        !write(9, "(4E24.16)") (hess(j,i),j=1,wnnprim)
        write(9, "(5d20.10)") (hess(i,j),j=1,wnnprim)

    end do

    if (debugflag .eqv. .true.) then
        
        print*, "hess"

        print "(4E24.16)", hess

    end if

    if (debugflag .eqv. .true.) then

        print*, "Writing rotated UMO to fort.99"
        !write(99, "(4E24.16)") UMO

        ! in same format as load_cmo.f90
        do i=1, wnnprim

            do j=1, wnnprim
                
                write(99, "(2I4, E24.16)") i, j, UMO(j, i)

            end do

        end do

    end if

    !print*, ""
    !print*, "DEBUG HESSIAN"
    !print*, ""

    !print*, "testing (3,3)"

    !print*, "central difference of two gradients"
    !print*, ""
    !call calc_grad(-1.0d-10, 1,3)

    !tmp1 = dFdT
    !call calc_grad(1.0d-10, 1,3)

    !tmp2 = dFdT


    !print*, "Numerical = ", (tmp2 - tmp1)/(2*1.0d-10)
    !print*, "analytic = ", hess(3,3)

    !print*, "testing (3,11)"

    !print*, "central difference of two gradients"
    !print*, ""

    !print*, "step = 1.0d-12"
    !call update_umos(1,11,1.0d-12)

    !call calc_grad(0, 1,3)
    !tmp1 = dFdT

    !call update_umos(1,11,-2.0d-12)

    !call calc_grad(0,1,3)
    !tmp2 = dFdT

    !print*, "tmp1", tmp1
    !print*, "tmp2", tmp2


    !print*, "Numerical = ", (tmp1 - tmp2)/(2*1.0d-12)

    !UMO = MO

    !print*, "step = 1.0d-10"
    !call update_umos(1,11,1.0d-10)

    !call calc_grad(0, 1,3)
    !tmp1 = dFdT

    !call update_umos(1,11,-2.0d-10)

    !call calc_grad(0,1,3)
    !tmp2 = dFdT

    !print*, "tmp1", tmp1
    !print*, "tmp2", tmp2


    !print*, "Numerical = ", (tmp1 - tmp2)/(2*1.0d-10)

    !UMO = MO


    !print*, "step = 1.0d-8"
    !call update_umos(1,11,1.0d-8)

    !call calc_grad(0, 1,3)
    !tmp1 = dFdT

    !call update_umos(1,11,-2.0d-8)

    !call calc_grad(0,1,3)
    !tmp2 = dFdT

    !print*, "tmp1", tmp1
    !print*, "tmp2", tmp2

    !print*, "Numerical = ", (tmp1 - tmp2)/(2*1.0d-8)


    !UMO = MO

    !print*, "step = 1.0d-6"
    !call update_umos(1,11,1.0d-6)

    !call calc_grad(0, 1,3)
    !tmp1 = dFdT

    !call update_umos(1,11,-2.0d-6)

    !call calc_grad(0,1,3)
    !tmp2 = dFdT

    !print*, "tmp1", tmp1
    !print*, "tmp2", tmp2

    !print*, "Numerical = ", (tmp1 - tmp2)/(2*1.0d-6)


    !UMO = MO

    !call full_hess(1,3,11,0d0,0d0)
     
    !print*, "analytical = ", hij



    !return

    !alphas = 0.0d0
    !alphas(3) = 1.0E-10
    !alphas(11) = 1.0E-10
    !call evalUdens3(dtol, .false., alphas, inocc)
    !call calcerrf(udens, refaodens, .false.)
    !deallocate(udens)

    !tmp1 = tmp1 + errf_value

    !alphas(3) = 1.0E-10
    !alphas(11) = -1.0E-10
    !call evalUdens3(dtol, .false., alphas, inocc)
    !call calcerrf(udens, refaodens, .false.)
    !deallocate(udens)

    !tmp1 = tmp1 - errf_value

    !alphas(3) = -1.0E-10
    !alphas(11) = 1.0E-10
    !call evalUdens3(dtol, .false., alphas, inocc)
    !call calcerrf(udens, refaodens, .false.)
    !deallocate(udens)

    !tmp1 = tmp1 - errf_value

    !alphas(3) = -1.0E-10
    !alphas(11) = -1.0E-10
    !call evalUdens3(dtol, .false., alphas, inocc)
    !call calcerrf(udens, refaodens, .false.)
    !deallocate(udens)

    !tmp1 = tmp1 + errf_value

    !print*, "f''(x) = ", tmp1/(4*1.0E-10*1.0E-10)


end subroutine opt_interface

