!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
! subroutine to minimize density differences by newton
! algorithm
subroutine newton(iter,inwrite, indebug)

    use orbital_variables

    use grid_variables

    use common_variables

    use sweep_variables

    implicit none

    logical                     :: inwrite
    logical                     :: indebug
    integer                     :: iter

    real*8                      :: alpha_sweep

    integer                     :: i

    integer                     ::  iocc, ivirt, newtoniter

    real*8                      :: current_err
    real*8                      :: previous_err
    real*8                      :: initial_err 

    real*8                      :: best_err

    ! to hold best alpha and MAD for each rotated MO
    real*8, dimension(:,:), allocatable :: alphas, best

    real*8                      :: NRsweep_time_start
    real*8                      :: NRsweep_time_finish

    real*8 :: modiff

    call cpu_time(NRsweep_time_start)

    ! checking if UMO is allocated
    if (allocated(UMO) .eqv. .false.) then
        print "(A30)", "Allocating UMO"

        allocate(UMO(wnnprim, wnnprim))

        ! copying MO to UMO
        UMO = MO

    end if

    ! find density with no rotation among orbitals
    call evalUdens2(dtol, .false., 0.0d0, 1, 2)

    call calcerrf(udens, refaodens, .true.)

    print "(A30)", "(what we want to beat)"

    deallocate(udens)

    ! initial, no rotation mad (what we want to beat)
    initial_err = errf_value

    if (initial_err .lt. sweeptol) then

        print "(A30,E24.16,A5,E24.16)", "Initial_err = ", initial_err, "< ", sweeptol
        print "(A30)", "exiting"

        return

    end if

    ! initial conditions
    previous_err = initial_err
    previous_best_sweep_err = initial_err

    ! allocating array to hold optimal alphas
    allocate(alphas(nocc,wnnprim))
    allocate(best(nocc,wnnprim))

    alphas = 0.0d0
    best = 0.0d0

    if (allocated(udens_copy) .eqv. .true.) then

        deallocate(udens_copy)

    end if

    allocate(udens_copy(npoints))

    print*, "*"
    print*, "*"
    print*, "*"
    print*, "================================================================"
    print "(A30)", "Newton sweep talking"
    print "(A30, A6)", "Minimize type = ", opttype
    print "(A30, I3)", "occ = ", nocc
    print "(A30, I8)", "maxiter = ", maxnewtoniter
    print "(A30, L)", "iterprint = ", print_iter
    print "(A30, L)", "update UMO = ", update_umo
    print "(A30, L)", "check ortho = ", ortho_check
    print*, "================================================================"
    print*, "*"
    print*, "*"
    print*, "*"

    ! occupied MO loop
    do iocc=1, nocc
        
        ! resetting these for each occ mo
        alphas = 0.0d0
        best = 0.0d0

        ! virtual MO loop
        do ivirt=nocc+1, wnnprim
        !do ivirt=wnnprim, nocc+1, -1
        
            ! check for same symmetry
            if (symcheck .eqv. .true.) then

                if (symlabels(iocc) .ne. symlabels(ivirt)) then

                    !if (print_iter .eqv. .true.) 
                    print "(A20, 2I4, A20)", "Skipping pair", iocc, ivirt, "due to symmetry"

                    ! save best mad equal to previous
                    best(iocc,ivirt) = best(iocc,ivirt-1)

                    ! set to zero
                    alphas(iocc,ivirt) = 0.0d0

                    cycle

                end if

            end if

            print*, ""
            print*, "***"
            print*, "=============================================="
            print "(A20, 2I4, A20, A20)", "Rotating pair", &
                & iocc, ivirt, "with virt symmetry", symlabels(ivirt) 
            print*, "***"
            print*, ""

            ! reset alpha
            alpha_sweep = 0.0d0

            ! reset NR step
            NR_step = 0.0d0

            ! reset trust
            trust = 1.0d0

            !best_err = current_err

            !current_err = best_err
            !previous_err = best_err

            !print*, "previous_err", previous_err
            !print*, "current err", current_err

            do newtoniter=1, maxnewtoniter

                call NRstep(alpha_sweep, iocc, ivirt, alphas)

  900           NR_step = NR_step*trust

                !print "(E24.16)", NR_step

                if (abs(NR_step) .lt. min_nr_step) then
                    
                    print*, "Step is too small"

                    !current_err = best_err

                    go to 901

                end if

                alpha_sweep = alpha_sweep + NR_step

                ! no write to file for each iteration
                call evalUdens2(dtol, .false., alpha_sweep, iocc, ivirt)

                ! if we want to check number of electrons
                call sumdens(udens, .false.)

                call calcerrf(udens, refaodens, .false.)

                current_err = errf_value

                deallocate(udens)

                if (print_iter .eqv. .true. ) then

                    print*, "====================="
                    print*, "Iteration information"
                    print "(A30, I3)", "iocc = ", iocc
                    print "(A30, I4)", "ivirt = ", ivirt
                    print "(A30, I8)", "newtoniter= ", newtoniter
                    print "(A30, E24.16)", "trust = ", trust
                    print "(A30, E24.16)", "Gradient = ", dFdT
                    print "(A30, E24.16)", "Hessian= ", dF2dT2
                    print "(A30, E24.16)", "NR step = ", NR_step
                    print "(A30, E24.16)", "Alpha = ", alpha_sweep
                    print "(A30, E24.16)", "Previous err = ", previous_err
                    print "(A30, E24.16)", "Current err = ", current_err
                    print*, "====================="

                end if

                if (current_err .gt. previous_err) then

                    if (print_iter .eqv. .true.) print*, "Current higher than previous, rejecting "
                    if (print_iter .eqv. .true.) print*, "Going back one step and halving trust radius"

                    alpha_sweep = alpha_sweep - NR_step

                    !current_err = previous_err

                    trust = trust * 0.5d0

                    goto 900

                else

                    ! successful NR step
                    !continue

                    ! convergence criteria for NR
                    if (abs(previous_err - current_err ) .lt. sweeptol) then

      901               print "(A30,I2,A30)", "Minima found in ", newtoniter, " NR iterations"

                        call gradhess(alpha_sweep,iocc,ivirt)
                        print "(A30, E24.16)", "Gradient = ", dFdT

                        ! save best mad
                        best(iocc,ivirt) = current_err

                        ! save optimal alpha
                        alphas(iocc,ivirt) = alpha_sweep
                        
                        exit

                    end if

                    ! update current
                    previous_err = current_err

                end if




                ! write walk to file
                !if (inwrite .eqv. .true.) then

                    !write(ivirt + 20, "(2E24.16)") alpha_sweep, current_err

                !end if

                ! for convergence testing
                !write(ivirt+20, "(I4,E24.16)") iter, mid_alpha
                !hist_alphas(iocc,ivirt,iter) = mid_alpha


            end do

            print*, ""

            print "(A30, E24.16)", "alpha = ", alphas(iocc,ivirt)
            print "(A30, E24.16)", "Best err = ", best(iocc,ivirt)
            print*, ""

            ! to check number of electrons are correct
            !call evalUdens2(innpoints, innnprim, 1.0d-10, .false., .false., alpha_sweep, ivirt+innocc)
            !call sumdens(innpoints, udens, grid)
            !deallocate(udens)

            if (update_umo .eqv. .true.) then

                print*, "Updated UMO"

                ! update CMO with optimal alpha
                call update_umos(iocc, ivirt, alphas(iocc,ivirt))

                if (print_iter .eqv. .true.) then

                    print "(A40,E24.16)", "Updated UMO based on alpha = ", alphas(iocc,ivirt)
                    print*, "=============================================="
                    print*, ""
                    print*, ""
                    print*, ""

                end if

                if (ortho_check .eqv. .true.) then

                    ! no need to print all this
                    call ortho_check_all("UMO", .false., orthotol)
                    print "(A32, E12.6, A10, 2I3)", "Largest deviation of overlap = ", &
                        & maxval(orthomat), "of pairs ", maxloc(orthomat)

                end if

            end if

            ! UMO diag
            if (UMOdiag .eqv. .TRUE. ) then

                print*, "UMO DIAG"
                print "(3A24)", "UMO(i, iocc)",  "refMO(i, iocc)", "diff(UMO-refCMO)"

                modiff = 0.0d0

                do i=1, wnnprim

                    modiff = modiff + abs(UMO(i,iocc) - refmo(i, iocc))

                    print "(3E24.16)", UMO(i, iocc), refmo(i, iocc), UMO(i, iocc) - refmo(i,iocc)

                end do

                print "(A24,E24.16)", "sum(modiff) = " , modiff

            end if

            print*, "=============================================="
            print*, ""

            ! update converged_alphas, incremented after each sweep
            converged_alphas(iocc,ivirt) = converged_alphas(iocc,ivirt) + alphas(iocc,ivirt)

        end do !ivirt

        print*, "*****"
        print "(A30, I4)", "Optimal alphas for iocc = ", iocc

        print "(A4, A15, 2A24)", "MO", "sym", "alpha", "MAD"
        do ivirt=1, wnnprim

            print "(I4, A15, 2E24.16)", ivirt, symlabels(ivirt), alphas(iocc,ivirt), best(iocc,ivirt)

        end do
        print*, "*****"

    end do !iocc

    call cpu_time(NRsweep_time_finish)

    ! saving best err
    best_sweep_err = best(nocc,wnnprim)

    print '(A30, f16.2)', "Walltime NR [s] = ", NRsweep_time_finish - NRsweep_time_start

    ! write to file fort.21 after each sweep
    open(unit=13, status = "replace", file="fort.21")

    write(13, "(4E24.16)") converged_alphas

    close(unit=13)
    

end subroutine newton


