MKLROOT = /comm/swstack/core/mkl/2019.2.187/mkl/

### gfortran
FASTFLAGS = -ffast-math -O3 -march='westmere' # q12
#FASTFLAGS = -ffast-math -O3 -march='sandybridge' # q16, q20
#FASTFLAGS = -ffast-math -O3 -march='skylake' # q36, q40, qfat

FFLAGS = -ffree-form  -Wall -g -fbacktrace -fcheck=all -fopenmp -std=gnu -freal-4-real-8



### ifort
#FFLAGS = -warn none -traceback -check all -qopenmp -real-size 64
#FASTFLAGS = -fast -O3 -march='sandybridge' # q16, q20

# identical to MRCC with ifort
#FFLAGS = -O3 -i8 -DOMP -DINT64 -assume byterecl -DIntel

# only needed for mkl link
LDLIBS = #-L${MKLROOT}lib/intel64 -Wl,--no-as-needed -lmkl_gf_ilp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl
LDLIBS = philip.a

#FC = ifort
FC = gfortran

#GIT_VERSION := $(shell git describe --abbrev=4 --dirty --always --tags)

#all: main_q12.x main_q16.x main_q28.x main_q36.x

default: rotmo.x

clean: 
	rm -f *.o rotmo.x *.mod *.txt philip.a

# MODULES
MODULES = grid_variables.o orbital_variables.o \
		  common_variables.o \
		  sweep_variables.o \
		  fit_variables.o

# objects containing subroutines
OBJS = parse_input.o load_lebedev.o integ.o nel.o\
	   load_dens.o construct_grid.o load_cmo.o cartfunctions.o\
	   load_basis.o load_1eD.o load_overlap.o errf.o\
	   dens.o line_sweep.o ortho.o load_sym.o lsq.o\
	   extrapolate.o restart.o grad.o newton_sweep.o \
	   opt_interface.o deriv.o rotmo.o

# compile all objects
%.o: %.f90
	$(FC) $(FFLAGS) $(FASTFLAGS) -c $*.f90

# link main program
rotmo.x: $(MODULES) $(OBJS)
	./library.make
	$(FC) $(FFLAGS) $(FASTFLAGS) -o rotmo.x rotmo.o $(LDLIBS)

