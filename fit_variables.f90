!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! module of common variables related to exponential fit (extrapolation)
!
module fit_variables

    implicit none

    public

    ! parse variables
    !

    ! type of git
    character(len=3)                                :: ex_type

    ! start and end indices of data points for extrapolation
    integer                                         :: ex_start
    integer                                         :: ex_end

    ! threshold for updating UMO based on R^2 value
    real*8                                          :: ex_tol

    ! number of iterations to extrapolate to
    integer                                         :: ex_num



    real*8, dimension(:), allocatable               :: fit_rcoeff
    real*8, dimension(:), allocatable               :: fit_rsterr
    real*8                                          :: fit_rsquared

    real*8, dimension(:,:,:), allocatable           :: paramlist

    real*8, dimension(:,:), allocatable             :: extrapolated_alphas


    save

end module
