ar -crsv philip.a \
parse_input.o \
load_lebedev.o \
integ.o \
nel.o \
load_dens.o \
construct_grid.o \
load_cmo.o \
cartfunctions.o \
grid_variables.o \
orbital_variables.o \
common_variables.o \
load_overlap.o \
load_basis.o \
load_1eD.o \
dens.o \
line_sweep.o \
ortho.o \
sweep_variables.o \
errf.o \
load_sym.o \
lsq.o \
fit_variables.o \
extrapolate.o \
restart.o \
grad.o \
newton_sweep.o \
opt_interface.o \
deriv.o \
rotmo.o

