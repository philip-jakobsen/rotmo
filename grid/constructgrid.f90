!=========================================================
!
! subroutine to create integration grid and write to file
! 
!==========================================================

subroutine collectgrid(inprintflag)

    use common_variables

    use grid_variables

    implicit none

    integer                     :: i
    logical                     :: inprintflag

    ! read angular grid from file "angular_grid/<order>"
    ! the weights are scaled accordingly
    call agrid(nagrid)

    ! construct grid and accompanying integration and fuzzy weights
    call makegrid(nrgrid, nagrid, lebedev_x, lebedev_y, lebedev_z, lebedev_w, natoms, atnums, coords)

    ! print cartesian grid to file fort.7
    if(inprintflag .eqv. .true.) then
        print*, "Writing ", npoints, "points to cartesian grid points to fort.7"
        do i=1, npoints
            write (7, '(4E24.16)') grid(1:4,i)
            !write (8, '(E20.10)') integration_weights(i)
        end do
        print*, "================================================================"
    end if

    print*, ""
    print*, ""
    print*, ""

end subroutine collectgrid

