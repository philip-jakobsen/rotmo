#!/bin/bash
# script to extract density from gamess output
# assuming the output and input filenames are the same

set -xe

outfile=$1

npoints=$(grep " BOHR" $outfile.inp | cut -c 6-)

npoints=$(echo "$npoints + 3" | bc )

grep -A $npoints "ELECTRON DENSITY" $outfile.out | tail -n +2 > $outfile.dens
