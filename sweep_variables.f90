!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! module of common variables related to sweep procedure
!
module sweep_variables

    implicit none

    public

    ! rotated MO
    real*8, dimension(:,:), allocatable             :: UMO

    ! number of doubly occupied orbitals
    integer                                         :: nocc

    ! tolerance for sweep, not used atm?
    real*8                                          :: sweeptol

    ! step size in radians for sweep 
    real*8                                          :: alpha_step

    ! convergence criteria for bisect, in radians
    real*8                                          :: bisecttol

    ! max number of line search iterations
    integer                                         :: maxlineiter

    ! max number of bisect iterations
    integer                                         :: maxbisectiter

    ! max number of sweep iterations
    integer                                         :: maxsweepiter

    ! flag to check orthonormality of MOs after rotation
    logical                                         :: ortho_check
    
    ! flag to print iteration information (both linesearch and bisect)
    logical                                         :: print_iter

    ! flag to update MOs after sweep 
    logical                                         :: update_umo

    ! flag for diagnostic of UMO when using a DFT as reference.
    ! prints rotated MOs, reference MOs and difference
    logical                                         :: UMOdiag

    ! flag to only sweep pairs with correct symmetry
    logical                                         :: symcheck
    
    ! array to hold angles (alphas) after a whole sweep (all occ-virt pairs)
    real*8, dimension(:,:), allocatable             :: converged_alphas

    ! array to hold previous alphas
    real*8, dimension(:,:), allocatable             :: converged_alphas_previous

    ! array to hold loaded alphas
    real*8, dimension(:,:), allocatable             :: converged_alphas_restart

    ! hold best sweep rmsd between sweep iterations
    real*8                                          :: best_sweep_err

    real*8                                          :: previous_best_sweep_err

    ! to hold history of alphas 
    real*8, dimension(:,:,:), allocatable           :: hist_alphas

    real*8, dimension(:), allocatable               :: hist_x

    ! NR variables
    real*8                                          :: NR_step, frj_NR_step

    real*8                                          :: dFdT, dF2dT2

    real*8                                          :: min_nr_step

    integer                                         :: maxnewtoniter

    real*8                                          :: trust

    ! BFGS variables
    integer                                         :: maxBFGSiter

    real*8                                          :: BFGStol

    ! full analytical hessian
    real*8                                          :: hij

    save

end module
