!=========================================================
!
! subroutine to load density from GAMESS calculation
! using the $POINTS directive
! 
! obviously assumes the same number of points as the grid
! from collectgrid subroutine
!
!==========================================================

subroutine loaddata(innpoints, indata_file_name)

    use grid_variables
    use orbital_variables

    implicit none

    integer                             :: i

    integer                             :: innpoints
    character(len=*)                    :: indata_file_name

    ! tmp array for reading npoint, x, y, z
    real*8                              :: tmpx, tmpy, tmpz

    print*, "================================================================"
    print*, "Loading reference density file from GAMESS"

    open(unit=13, file=indata_file_name, form='formatted')

    allocate(refdens(innpoints))

    ! empty read to skip first two lines
    !read(23,*) 
    !read(23,*) 

    do i=1,innpoints
        read(13, "(4E24.16)") tmpx, tmpy, tmpz, refdens(i)
    end do

    close(unit=13)


end subroutine loaddata


