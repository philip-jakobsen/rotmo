!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutines to calculate analytical gradient vector
!
subroutine calc_grad_vec(inocc, inalphas, outvec)

    use sweep_variables

    use orbital_variables

    implicit none

    integer :: inocc

    real*8, dimension(wnnprim) :: inalphas

    real*8, dimension(wnnprim) :: outvec

    integer :: ivirt

    ! construct gradient vector
    do ivirt=inocc+1, wnnprim

        if (symcheck .eqv. .true.) then

            if (symlabels(inocc) .ne. symlabels(ivirt)) then

                !print "(A20, 2I4, A20)", "Skipping pair", iocc, ivirt, "due to symmetry"

                cycle

            end if

        end if

        ! new, rotating all other orbitals
        !call calc_grad3(inalphas, inocc, ivirt)

        !outvec(ivirt) = dFdT

        call calc_grad4(inalphas, inocc, ivirt)
        outvec(ivirt) = dFdT

    end do

end subroutine calc_grad_vec

! subroutines to calculate analytical hessian matrix
subroutine calc_hess_mat(inocc, inalphas, outmat, debugflag)

    use orbital_variables

    use sweep_variables

    use grid_variables

    use common_variables

    implicit none

    logical :: debugflag

    integer :: inocc

    real*8, dimension(wnnprim) :: inalphas

    real*8, dimension(wnnprim,wnnprim) :: outmat

    integer :: ivirt, jvirt

    integer :: i

    outmat = 0.0d0

    ! initialize with large identity, to level-shift away the frozen variables
    do i=1, wnnprim
        outmat(i,i) = 1.0d+08
    end do

!    do ivirt=inocc+1,wnnprim
!
!        do jvirt=inocc+1,wnnprim
!
!            if (symcheck .eqv. .true.) then
!
!                if (symlabels(inocc) .ne. symlabels(ivirt) .or. symlabels(inocc) .ne. symlabels(jvirt) ) then
!
!                    !print "(A20, 2I4, A20)", "Skipping pair", ivirt, jvirt, "due to symmetry"
!
!                    cycle
!
!                end if
!
!            end if
!
!            ! original
!            !call full_hess(inocc,ivirt,jvirt,inalphas(ivirt), inalphas(jvirt))
!
!            ! new, rotating all other orbitals 
!            call full_hess3(inocc,ivirt,jvirt,inalphas)
!
!            outmat(ivirt,jvirt) = hij
!
!        end do
!
!    end do

    !print*,"FRJ, deriv, hess3"
    !do i=1,wnnprim
    !  print "(14E12.4)",(outmat(i,:))
    !end do

    do ivirt=inocc+1,wnnprim
        do jvirt=inocc+1,wnnprim
            if (symcheck .eqv. .true.) then
                if (symlabels(inocc) .ne. symlabels(ivirt) .or. symlabels(inocc) .ne. symlabels(jvirt) ) then
                    cycle
                end if
            end if
             call full_hess4(inocc,ivirt,jvirt,inalphas)
             outmat(ivirt,jvirt) = hij
        end do
    end do
    !print*,"FRJ, deriv, hess4"
    !do i=1,wnnprim
    !  print "(14E12.4)",(outmat(i,:))
    !end do

end subroutine calc_hess_mat

