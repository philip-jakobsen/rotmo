module grid_variables

    implicit none

    public

    ! first used in parseinp
    character(len=50)                   :: data_file_name
    integer                             :: nrgrid, nagrid
    integer                             :: npoints

    ! first used in load_lebedev
    real*8, dimension(:), allocatable   :: lebedev_x, lebedev_y, lebedev_z, lebedev_w

    ! first used in integ
    real*8, dimension(:,:), allocatable :: grid

    ! first used in loaddens
    real*8, dimension(:), allocatable   :: data_array

    ! first used in nel
    real*8                              :: nelectrons

    save

end module

