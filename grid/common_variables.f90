module common_variables

    implicit none

    public

    integer                                         :: natoms
    real*8, dimension(:,:), allocatable             :: coords
    integer, dimension(:), allocatable              :: atnums
    character(len=5), dimension(:), allocatable     :: atlabel

    character(len=50)                               :: basisdir
    character(len=50), dimension(:), allocatable    :: basisfilename
    character(len=50), dimension(:), allocatable    :: refbasisfilename
    character(len=50)                               :: mopun
    character(len=50)                               :: refmopun
    character(len=50)                               :: overlapfilename
    character(len=50)                               :: densitymatrixfilename
    character(len=50)                               :: refdensitymatrixfilename

    logical                                         :: sweepflag

    ! mad stuff
    real*8                                          :: mad_value

    real*8                                          :: rmsd_value

    save

end module
