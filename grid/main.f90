! IO units 5 = stdin (parseinp.f90)
! 6 = stdout

! 1x = DALTON
!
! 10 = dalton basis file (loadbasis.f90)
! 11 = DALTON.MOPUN (AO -> MO coefficients) (loadmos.f90)
! 12 = dalton overlap matrix (fort.50) (loadoverlap.f90)
! 13 = dalton (lucita) 1e density matrix (fort.91) (load1eD.f90)
! 14 = density on grid output (main.f90)
! 15 = reference density on grid output (main.f90)
! 15 = rotated density on grid output (main.f90)

! 16 = reference DALTON.MOPUN (AO -> MO coefficients) (loadmos.f90)

! 2x = GAMESS
! 23 = gamess density output (loaddens.f90)

program main

    use common_variables

    use grid_variables

    implicit none

    integer :: i, j

    logical :: debug, test, printflag, writeflag, evald, evalrefd, orthonorm_check, loadref
    
    real*8 :: pi
    integer :: fileidx
    
    real*8 :: tmp, tmpmo1, tmpmo2

    real*8 :: alpha_test

    real*8 :: evalcartfunc2, normonecart, normonecart2

    integer :: doublefact

    real*8 :: ypt

    real*8, dimension(3) :: coor

    ! misc values
    printflag = .false.

    ! read input file from stdin
    call parse

    ! constructing grid writes x y z to fort.7
    call collectgrid(nrgrid, nagrid, natoms, npoints, atnums, coords, printflag)


end program main




