!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to calculate ErrF, normalized by number of electrons
subroutine calcerrf(indens, inrefdens, inprint)

    use common_variables

    use grid_variables

    implicit none

    ! input parameters
    real*8, dimension(npoints)          :: inrefdens
    real*8, dimension(npoints)          :: indens
    real*8                              :: Nerrf

    logical                             :: inprint

    ErrF_value = 0.0d0

    Nerrf = sum(inrefdens*grid(4,:))**2.0

    ErrF_value = sum((inrefdens-indens)**2*grid(4,:)) / Nerrf

    ! OLD
    !ErrF_value = sum((inrefdens-indens)**2) / dble(sum(atnums))

    if (inprint .eqv. .true. ) then

        print*, "================================================================"
        print '(A10, E24.16)', "ErrF = ", ErrF_value
        print*, ""

    end if

end subroutine

! subroutine to calculate Root Mean Square Deviation
subroutine calcrmsd(indens, inrefdens, inprint)

    use common_variables

    use grid_variables

    implicit none

    ! input parameters
    real*8, dimension(npoints)          :: inrefdens
    real*8, dimension(npoints)          :: indens

    logical                             :: inprint

    integer                             :: i

    rmsd_value = sum((inrefdens - indens)**2)

    rmsd_value = sqrt(rmsd_value) / dble(npoints)

    if (inprint .eqv. .true. ) then

        print*, "================================================================"
        print '(A10, E24.16)', "RMSD  = ", rmsd_value
        print*, ""

    end if

end subroutine

! subroutine to calculate Mean Absolute Deviation
subroutine calcmad(indens, inrefdens, inprint)

    use common_variables

    use grid_variables

    implicit none

    ! input parameters
    real*8, dimension(npoints)          :: inrefdens
    real*8, dimension(npoints)          :: indens

    real*8                              :: mean

    integer :: i

    logical                             :: inprint

    ! calculating the mean
    mean = 1.0d0 / dble(npoints) * sum(abs(indens - inrefdens)) 
    !print '(A10, E20.10)', "mean = ", mean

    mad_value = 0.0d0

    do i=1, npoints
        
        mad_value = mad_value + abs(indens(i) - mean)

    end do

    mad_value = mad_value / dble(npoints)
        

    if (inprint .eqv. .true. ) then
        print*, "================================================================"
        print '(A10, E24.16)', "MAD = ", mad_value
        print*, ""
    end if

end subroutine


