#!/bin/bash

# script to add points from fort.7 file to gamess input file
# gamess input file MUST end with $END from previous block
#
# also assumes the fort.7 file and the gamess input
# is on the same directory

set -xe

gamessinp=$1

npoints=$(wc -l < "fort.7" )

echo " \$POINTS" >> $gamessinp

echo " BOHR $npoints" >> $gamessinp

cat fort.7 >> $gamessinp

echo " \$END" >> $gamessinp


